The Sonos Content API Sample Server
===================================

The Sonos Content API Sample Server provides a minimal sample implementation of a Content API service. The server is built and run using gradle.

For documentation on the Sonos Content API, please sign up on [Sonos Labs](http://musicpartners.sonos.com/) and read the documentation.


# Requirements

* Java 8
* Gradle


# Building the server

To build the Content API Sample Server issue the following command in the root directory of the project:

```sh
./gradlew build
```

You can also build it with Intellij by creating a project from existing sources and loading the root directory of the project.

# Running the server

To run the Content API Sample Server issue the following command in the root directory of the project:

```sh
./gradlew bootRun
```

This command will both build and start the server. The server should be visible at `http://localhost:8080`. If you would like to use another port, create an `application.properties` file containing the line:

In the console log, look for an "Started Application in [time]" message. The Spring Boot status line will remain at the bottom of the console log and may continue to display a message like "Building 85%", but the server will be running.

```
server.port: <PORT_NUM>
```

You can verify that the server is running by sending a SOAP command in request.xml. Issue the
following `curl` command:

```sh
curl --header "content-type: text/xml" -d @request.xml http://localhost:8080/musicservice
```

### Getting the Content API WSDL File

You can also query the server for the WSDL file. The following command will return WSDL file:

```sh
curl http://localhost:8080/musicservice/contentapi.wsdl
```

### Getting the Strings and Presentation Map XML Files

```sh
curl http://localhost:8080/static/strings.xml
curl http://localhost:8080/static/presentationmap.xml
```

# Adding your service to a Sonos system

To add the Acme service provided by your running Content API server to an existing Sonos system, you can use the customSD mechanism as described on Sonos Labs article ["Add your service with customSD"](http://musicpartners.sonos.com/?q=node/134).

- Endpoint and Secure Endpoint URLs should be `http://<YOUR_SERVER_IP>:8080/musicservice`
- Authentication SOAP header policy should be "Device Link".
- String table Uri should be `http://<YOUR_SERVER_IP>:8080/static/strings.xml`
- Presentation Map Uri should be `http://<YOUR_SERVER_IP>:8080/static/presentationmap.xml`
- Container Type should be "Music Service".
- The "Search" and "Extended Metadata" Capabilities checkboxes should be checked.

After configuring the custom service, you should see it listed, with the name you entered on the customsd.htm form, in "Add music service" in the Sonos controller. Select it and follow the Device Link authorization flow. The Content API Sample Server is not intended as a demonstration of user account management practices, so it only recognizes a single, hardcoded account, with username `acmeUser` and password `password`.

# Revision History

- Version 0.1: Initial release
