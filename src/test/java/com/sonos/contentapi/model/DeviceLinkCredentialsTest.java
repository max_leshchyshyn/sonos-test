/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DeviceLinkCredentialsTest {
    private DeviceLinkCredentials credentials;
    private static final long AUTH_DURATION = 300000;

    @Before
    public void setUp() throws Exception {
        credentials = new DeviceLinkCredentials();
        credentials.setAuthKey("authKey");
        credentials.setAuthToken("authToken");
        credentials.setHouseholdId("householdId");
        credentials.setLinkDeviceId("linkDeviceId");
    }

    @Test
    public void generateAuthToken() throws Exception {
        long approxExpectedValue = System.currentTimeMillis() + AUTH_DURATION;
        String testAuthTokenString = DeviceLinkCredentials.generateAuthToken();
        long testAuthTokenValue = Long.parseLong(testAuthTokenString);
        // let's be generous and allow it to take up to a second to "generate" the auth token
        assertTrue(testAuthTokenValue - approxExpectedValue <= 1000);
    }

    @Test
    public void getLinkDeviceId() throws Exception {
        assertEquals("linkDeviceId", credentials.getLinkDeviceId());
    }

    @Test
    public void setLinkDeviceId() throws Exception {
        credentials.setLinkDeviceId("newLinkDeviceId");
        assertEquals("newLinkDeviceId", credentials.getLinkDeviceId());
    }

    @Test
    public void getHHId() throws Exception {
        assertEquals("householdId", credentials.getHouseholdId());
    }

    @Test
    public void setHHId() throws Exception {
        credentials.setHouseholdId("newHouseholdId");
        assertEquals("newHouseholdId", credentials.getHouseholdId());
    }

    @Test
    public void getAuthToken() throws Exception {
        assertEquals("authToken", credentials.getAuthToken());
    }

    @Test
    public void setAuthToken() throws Exception {
        credentials.setAuthToken("newAuthToken");
        assertEquals("newAuthToken", credentials.getAuthToken());
    }

    @Test
    public void getAuthKey() throws Exception {
        assertEquals("authKey", credentials.getAuthKey());
    }

    @Test
    public void setAuthKey() throws Exception {
        credentials.setAuthKey("newAuthKey");
        assertEquals("newAuthKey", credentials.getAuthKey());
    }

}