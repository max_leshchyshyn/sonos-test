/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Track;
import com.sonos.services._1.GetMetadata;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.soap.SoapMessageException;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.ResponseMatcher;
import org.springframework.xml.transform.StringSource;
import org.w3c.dom.NodeList;

import javax.xml.transform.Source;
import java.io.IOException;
import java.util.Locale;

import static com.sonos.contentapi.model.enums.ObjectId.*;
import static com.sonos.contentapi.model.enums.ObjectIdPrefix.*;
import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.RequestCreators.withSoapEnvelope;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MusicServiceConfig.class,
        MusicServiceEndpoint.class,
        MusicServiceGetMetadataEndpoint.class,
        MusicServiceSearchEndpoint.class})
public class MusicServiceGetMetadataEndpointTests extends CredentialedApiTestsBase {
    private static final String METHOD_UNDER_TEST = "getMetadata";

    private static final String GET_METADATA_REQUEST_FMT = "<ns:%1$s xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">" +
            "<ns:id>%2$s</ns:id>" +
            "<ns:index>%3$s</ns:index>" +
            "<ns:count>%4$s</ns:count>" +
            "</ns:%1$s>";
    private static final String GET_METADATA_SEARCH_RESULT = "<ns:getMetadataResponse xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">" +
            "<ns:getMetadataResult>" +
            " <ns:index>0</ns:index>" +
            " <ns:count>3</ns:count>" +
            " <ns:total>3</ns:total>" +
            " <ns:mediaCollection>" +
            "  <ns:id>artists</ns:id>" +
            "  <ns:title>Artists</ns:title>" +
            "  <ns:itemType>search</ns:itemType>" +
            " </ns:mediaCollection>" +
            " <ns:mediaCollection>" +
            "  <ns:id>albums</ns:id>" +
            "  <ns:title>Albums</ns:title>" +
            "  <ns:itemType>search</ns:itemType>" +
            " </ns:mediaCollection>" +
            " <ns:mediaCollection>" +
            "  <ns:id>tracks</ns:id>" +
            "  <ns:title>Tracks</ns:title>" +
            "  <ns:itemType>search</ns:itemType>" +
            " </ns:mediaCollection>" +
            "</ns:getMetadataResult>" +
            "</ns:getMetadataResponse>";

    @Autowired
    private ApplicationContext applicationContext;

    private MockWebServiceClient mockClient;

    @Before
    public void createClient() {
        mockClient = MockWebServiceClient.createClient(applicationContext);
        authorizeWithService();
    }

    @After
    public void cleanUpAfterTest() {
        authToken = null;
        refreshToken = null;
    }

    @Test
    public void getMetaDataNoId() {
        testUnimplementedMethod(mockClient, buildHeader(), new String[]{"getMetadata"});
    }

    @Test
    public void getMetadataRootEn() {
        Locale.setDefault(new Locale("en"));

        String responsePayload = "<ns2:getMetadataResponse xmlns:ns2=\"http://www.sonos.com/Services/1.1\"><ns2:getMetadataResult><ns2:index>0</ns2:index><ns2:count>3</ns2:count><ns2:total>3</ns2:total><ns2:mediaCollection><ns2:id>browse:albums</ns2:id><ns2:itemType>album</ns2:itemType><ns2:title>Albums</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:artists</ns2:id><ns2:itemType>artist</ns2:itemType><ns2:title>Artists</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:tracks</ns2:id><ns2:itemType>container</ns2:itemType><ns2:title>Tracks</ns2:title></ns2:mediaCollection></ns2:getMetadataResult></ns2:getMetadataResponse>";
        _testObjectIdBinding(ROOT, responsePayload);
    }

    @Test
    public void getMetadataRootDe() {
        Locale.setDefault(new Locale("de"));

        String responsePayload = "<ns2:getMetadataResponse xmlns:ns2=\"http://www.sonos.com/Services/1.1\"><ns2:getMetadataResult><ns2:index>0</ns2:index><ns2:count>3</ns2:count><ns2:total>3</ns2:total><ns2:mediaCollection><ns2:id>browse:albums</ns2:id><ns2:itemType>album</ns2:itemType><ns2:title>Alben</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:artists</ns2:id><ns2:itemType>artist</ns2:itemType><ns2:title>Interpreten</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:tracks</ns2:id><ns2:itemType>container</ns2:itemType><ns2:title>Titel</ns2:title></ns2:mediaCollection></ns2:getMetadataResult></ns2:getMetadataResponse>";
        _testObjectIdBinding(ROOT, responsePayload);
    }

    @Test
    public void getMetadataRootEs() {
        Locale.setDefault(new Locale("es"));

        String responsePayload = "<ns2:getMetadataResponse xmlns:ns2=\"http://www.sonos.com/Services/1.1\"><ns2:getMetadataResult><ns2:index>0</ns2:index><ns2:count>3</ns2:count><ns2:total>3</ns2:total><ns2:mediaCollection><ns2:id>browse:albums</ns2:id><ns2:itemType>album</ns2:itemType><ns2:title>Álbumes</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:artists</ns2:id><ns2:itemType>artist</ns2:itemType><ns2:title>Artistas</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:tracks</ns2:id><ns2:itemType>container</ns2:itemType><ns2:title>Pistas</ns2:title></ns2:mediaCollection></ns2:getMetadataResult></ns2:getMetadataResponse>";
        _testObjectIdBinding(ROOT, responsePayload);
    }

    @Test
    public void getMetadataRootZh() {
        Locale.setDefault(new Locale("zh"));

        String responsePayload = "<ns2:getMetadataResponse xmlns:ns2=\"http://www.sonos.com/Services/1.1\"><ns2:getMetadataResult><ns2:index>0</ns2:index><ns2:count>3</ns2:count><ns2:total>3</ns2:total><ns2:mediaCollection><ns2:id>browse:albums</ns2:id><ns2:itemType>album</ns2:itemType><ns2:title>专辑</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:artists</ns2:id><ns2:itemType>artist</ns2:itemType><ns2:title>表演者</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:tracks</ns2:id><ns2:itemType>container</ns2:itemType><ns2:title>曲目</ns2:title></ns2:mediaCollection></ns2:getMetadataResult></ns2:getMetadataResponse>";
        _testObjectIdBinding(ROOT, responsePayload);
    }

    @Test
    public void getMetadataRootJa() {
        Locale.setDefault(new Locale("ja"));

        String responsePayload = "<ns2:getMetadataResponse xmlns:ns2=\"http://www.sonos.com/Services/1.1\"><ns2:getMetadataResult><ns2:index>0</ns2:index><ns2:count>3</ns2:count><ns2:total>3</ns2:total><ns2:mediaCollection><ns2:id>browse:albums</ns2:id><ns2:itemType>album</ns2:itemType><ns2:title>アルバム</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:artists</ns2:id><ns2:itemType>artist</ns2:itemType><ns2:title>アーティスト</ns2:title></ns2:mediaCollection><ns2:mediaCollection><ns2:id>browse:tracks</ns2:id><ns2:itemType>container</ns2:itemType><ns2:title>トラック</ns2:title></ns2:mediaCollection></ns2:getMetadataResult></ns2:getMetadataResponse>";
        _testObjectIdBinding(ROOT, responsePayload);
    }


    @Test(expected = SoapMessageException.class)
    public void getMetadataRootBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataRoot(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataSearch() {
        Source envelopeSource = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                buildHeader(), String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "search", "0", "100")));
        Source resultSource = new StringSource(GET_METADATA_SEARCH_RESULT);
        mockClient.sendRequest(withSoapEnvelope(envelopeSource)).andExpect(payload(resultSource));
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataSearchBadId() {
        new MusicServiceSearchEndpoint().getMetadataSearch(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataBrowseAlbums() {
        GetMetadataResponseMatcher matcher = new GetMetadataResponseMatcher();
        matcher.responseName = "getMetadataResponse";
        matcher.resultName = "getMetadataResult";
        ContentApiDaoInterface dao = new ContentApiDao();
        // add 3 for the "index", "count", and "total" fields
        matcher.numChildren = Math.min(53, dao.getAllCount(Album.class) + 3);
        String req = String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "browse:albums", "0", "50");
        String env = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), req);
        mockClient.sendRequest(withSoapEnvelope(new StringSource(env))).andExpect(matcher);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataBrowseAlbumsBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataBrowseAlbums(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataBrowseAlbumsV2() {
        _testObjectIdBindingEmptyResponse(BROWSE_ALBUMS_V2);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataBrowseAlbumsV2BadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataBrowseAlbums_V2(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataBrowseArtists() {
        GetMetadataResponseMatcher matcher = new GetMetadataResponseMatcher();
        matcher.responseName = "getMetadataResponse";
        matcher.resultName = "getMetadataResult";
        ContentApiDaoInterface dao = new ContentApiDao();
        // add 3 for the "index", "count", and "total" fields
        matcher.numChildren = Math.min(53, dao.getAllCount(Artist.class) + 3);
        String req = String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "browse:artists", "0", "50");
        String env = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), req);
        mockClient.sendRequest(withSoapEnvelope(new StringSource(env))).andExpect(matcher);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataBrowseArtistsBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataBrowseArtists(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataBrowseRadio() {
        _testObjectIdUnimplementedRoute(BROWSE_RADIO);
    }

    @Test
    public void getMetadataBrowseTracks() {
        GetMetadataResponseMatcher matcher = new GetMetadataResponseMatcher();
        matcher.responseName = "getMetadataResponse";
        matcher.resultName = "getMetadataResult";
        ContentApiDaoInterface dao = new ContentApiDao();
        // add 3 for the "index", "count", and "total" fields
        matcher.numChildren = Math.min(53, dao.getAllCount(Track.class) + 3);

        String req = String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "browse:tracks", "0", "50");
        String env = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), req);
        mockClient.sendRequest(withSoapEnvelope(new StringSource(env))).andExpect(matcher);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataBrowseTracksBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataBrowseTracks(_getGetMetadataWithBadObjectId());
    }

    @Test
    public void getMetadataBrowseTOTM() {
        _testObjectIdUnimplementedRoute(BROWSE_TOTM);
    }

    @Test
    public void getMetadataBrowseListNow() {
        _testObjectIdUnimplementedRoute(BROWSE_LISTEN_NOW);
    }

    @Test
    public void getMetadataBrowsePlaylists() {
        _testObjectIdUnimplementedRoute(BROWSE_PLAYLISTS);
    }

    @Test
    public void getMetadataBrowseFavorites() {
        _testObjectIdUnimplementedRoute(BROWSE_FAVORITES);
    }

    @Test
    public void getMetadataBrowseFavoriteTracks() {
        _testObjectIdUnimplementedRoute(BROWSE_FAVORITE_TRACKS);
    }

    @Test
    public void getMetadataBrowseFavoriteAlbums() {
        _testObjectIdUnimplementedRoute(BROWSE_FAVORITE_ALBUMS);
    }

    @Test
    public void getMetadataBrowseConcierge() {
        _testObjectIdUnimplementedRoute(BROWSE_CONCIERGE);
    }

    @Test
    public void getMetadataConciergeGenre() {
        _testObjectIdUnimplementedRoute(CONCIERGE_GENRE);
    }

    @Test
    public void getMetadataPrefixAlbum() {
        GetMetadataResponseMatcher matcher = new GetMetadataResponseMatcher();
        matcher.responseName = "getMetadataResponse";
        matcher.resultName = "getMetadataResult";
        ContentApiDaoInterface dao = new ContentApiDao();
        // add 3 for the "index", "count", and "total" fields
        matcher.numChildren = Math.min(53, dao.getCount(Track.class, "album.id", 3) + 3);

        String req = String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "al:3", "0", "50");
        String env = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), req);
        mockClient.sendRequest(withSoapEnvelope(new StringSource(env))).andExpect(matcher);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataPrefixAlbumBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataPrefixAlbum(_getGetMetadataWithBadObjectId());
    }

    @Test(expected = AssertionError.class)
    public void getMetadataPrefixAlbumAnchor() {
        _testObjectIdPrefixBinding("pre" + ALBUM);
    }

    @Test
    public void getMetadataPrefixArtist() {
        GetMetadataResponseMatcher matcher = new GetMetadataResponseMatcher();
        matcher.responseName = "getMetadataResponse";
        matcher.resultName = "getMetadataResult";
        ContentApiDaoInterface dao = new ContentApiDao();
        // add 3 for the "index", "count", and "total" fields
        matcher.numChildren = Math.min(53, dao.getCount(Album.class, "artist.id", 5) + 3);

        String req = String.format(GET_METADATA_REQUEST_FMT, "getMetadata", "ar:5", "0", "50");
        String env = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), req);
        mockClient.sendRequest(withSoapEnvelope(new StringSource(env))).andExpect(matcher);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataPrefixArtistBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataPrefixArtist(_getGetMetadataWithBadObjectId());
    }

    @Test(expected = AssertionError.class)
    public void getMetadataPrefixArtistAnchor() {
        _testObjectIdPrefixBinding("pre" + ARTIST);
    }

    @Test
    public void getMetadataPrefixPlaylist() {
        _testObjectIdPrefixBinding(PLAYLIST);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataPrefixPlaylistBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataPrefixPlaylist(_getGetMetadataWithBadObjectId());
    }

    @Test(expected = AssertionError.class)
    public void getMetadataPrefixPlaylistAnchor() {
        _testObjectIdPrefixBinding("pre" + PLAYLIST);
    }

    @Test
    public void getMetadataPrefixProgramRadio() {
        _testObjectIdPrefixBinding(PROGRAM_RADIO);
    }

    @Test(expected = SoapMessageException.class)
    public void getMetadataPrefixProgramRadioBadId() {
        new MusicServiceGetMetadataEndpoint().getMetadataPrefixProgramRadio(_getGetMetadataWithBadObjectId());
    }

    @Test(expected = AssertionError.class)
    public void getMetadataPrefixProgramRadioAnchor() {
        _testObjectIdPrefixBinding("pre" + PROGRAM_RADIO);
    }

    private void _testObjectIdBindingEmptyResponse(String id) {
        _testObjectIdBinding(id, String.format(EMPTY_XML_PAYLOAD, METHOD_UNDER_TEST + "Response"));
    }

    private void _testObjectIdUnimplementedRoute(String id) {
        _testObjectIdBinding(id, String.format(MusicServiceEndpointTests.UNIMPLEMENTED_RESPONSE, "getMetadata"));
    }

    private void _testObjectIdBinding(String id, String response) {
        Source requestEnvelope = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                buildHeader(), String.format(XML_PAYLOAD_WITH_ID, METHOD_UNDER_TEST, id)));
        Source responsePayload = new StringSource(response);

        mockClient.sendRequest(withSoapEnvelope(requestEnvelope)).andExpect(payload(responsePayload));
    }

    private void _testObjectIdPrefixBinding(String prefix) {
        _testObjectIdBindingEmptyResponse(prefix + "12345");
    }

    private GetMetadata _getGetMetadataWithBadObjectId() {
        return new GetMetadata() {{
            setId("bad:id");
        }};
    }

    class GetMetadataResponseMatcher implements ResponseMatcher {
        String responseName;
        String resultName;
        int numChildren;

        @Override
        public void match(WebServiceMessage request, WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            NodeList children = MusicServiceEndpointTests.basicResponseChecks(src, responseName, resultName, numChildren);
            assert children != null;
        }
    }
}
