package com.sonos.contentapi.util.media.builders;

import com.sonos.contentapi.MusicServiceConfig;
import com.sonos.services._1.MediaCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Locale;

import static com.sonos.contentapi.model.enums.ObjectId.BROWSE_ALBUMS;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by aknight on 5/13/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MusicServiceConfig.class})
public class MediaCollectionBuilderTests {

    private Locale originalDefaultLocale = Locale.getDefault();

    @Autowired
    MessageSource source;

    @Before
    public void setUp() {
        Locale.setDefault(new Locale("en"));
    }

    @After
    public void teardown() {
        Locale.setDefault(originalDefaultLocale);
    }

    @Test
    public void nullMessageSource() {
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(null).title("Title").build();

        assertEquals("Title", collection.getTitle());
    }

    @Test
    public void nullTitle() {
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(source).title(null).build();
        assertEquals(null, collection.getTitle());
    }

    @Test
    public void englishTitle() {
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(source).title(BROWSE_ALBUMS).build();
        assertEquals("Albums", collection.getTitle());
    }

    @Test
    public void germanTitle() {
        Locale.setDefault(new Locale("de"));
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(source).title(BROWSE_ALBUMS).build();
        assertEquals("Alben", collection.getTitle());
    }

    @Test
    public void japaneseTitle() {
        Locale.setDefault(new Locale("ja"));
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(source).title(BROWSE_ALBUMS).build();
        assertEquals("アルバム", collection.getTitle());
    }

    @Test
    public void noLocalizationTitle() {
        Locale.setDefault(new Locale("de"));
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection().title(BROWSE_ALBUMS).build();

        assertEquals(BROWSE_ALBUMS, collection.getTitle());
    }
    @Test
    public void nullSummary() {
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection(source).title(null).build();
        assertEquals(null, collection.getTitle());
    }

    @Test
    public void noLocalizationSummary() {
        Locale.setDefault(new Locale("de"));
        MediaCollection collection = MediaCollectionBuilder.buildMediaCollection().title(BROWSE_ALBUMS).build();

        assertEquals(BROWSE_ALBUMS, collection.getTitle());
    }


}
