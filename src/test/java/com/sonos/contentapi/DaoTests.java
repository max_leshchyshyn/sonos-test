/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static com.sonos.contentapi.model.enums.RelatedTextType.ALBUM_NOTES;
import static com.sonos.contentapi.model.enums.RelatedTextType.ARTIST_BIO;

public class DaoTests {
    private ContentApiDaoInterface contentApiDaoInterface = new ContentApiDao();

    @Test
    public void testGetAllTracks() {
        List<Track> tracks = contentApiDaoInterface.getTracks(0,500);
        Assert.assertEquals("Dao should return " + Integer.toString(contentApiDaoInterface.getAllCount(Track.class)) +
                " tracks", tracks.size(), contentApiDaoInterface.getAllCount(Track.class));
    }

    @Test
    public void testGetSpecificTrack() {
        List<Track> tracks = contentApiDaoInterface.getSearchTracks("", "56", 0, 100);
        Assert.assertEquals("Dao should return 1 track", 1, tracks.size());
        List<Track> allTracks = contentApiDaoInterface.getTracks(0, contentApiDaoInterface.getAllCount(Track.class));
        Assert.assertEquals(allTracks.get(56), tracks.get(0));
    }

    @Test
    public void testGetAllAlbums() {
        List<Album> albums = contentApiDaoInterface.getAlbums(0,500);
        Assert.assertEquals("Dao should return " + Integer.toString(contentApiDaoInterface.getAllCount(Album.class)) +
                " albums", albums.size(), contentApiDaoInterface.getAllCount(Album.class));
    }

    @Test
    public void testGetSpecificAlbums() {
        List<Album> albums = contentApiDaoInterface.getSearchAlbums("", "56", 0, 100);
        Assert.assertEquals("Dao should return 1 album", 1, albums.size());
        List<Album> allAlbums = contentApiDaoInterface.getAlbums(0, contentApiDaoInterface.getAllCount(Album.class));
        Album album = contentApiDaoInterface.getAlbum(56);
        Assert.assertEquals(album,albums.get(0));
        Assert.assertEquals(album, allAlbums.get(56));
    }

    @Test
    public void testGetNonExistentAlbum() {
        Album album = contentApiDaoInterface.getAlbum(-1);
        Assert.assertEquals(null, album);
    }

    @Test
    public void testGetAllArtists() {
        List<Artist> artists = contentApiDaoInterface.getArtists(0, 500);
        Assert.assertEquals("Dao should return " + Integer.toString(contentApiDaoInterface.getAllCount(Artist.class)) +
                " artists", artists.size(), contentApiDaoInterface.getAllCount(Artist.class));
    }

    @Test
    public void testGetSpecificArtist() {
        List<Artist> artists = contentApiDaoInterface.getSearchArtists("", "50", 0, 100);
        Assert.assertEquals("Dao should return 1 artist", 1, artists.size());
        List<Artist> allArtists = contentApiDaoInterface.getArtists(0, contentApiDaoInterface.getAllCount(Artist.class));
        Artist artist = contentApiDaoInterface.getArtist(50);
        Assert.assertEquals(artist, artists.get(0));
        Assert.assertEquals(artist, allArtists.get(50));
    }

    @Test
    public void testGetNonExistentArtist() {
        Artist artist = contentApiDaoInterface.getArtist(-1);
        Assert.assertEquals(null, artist);
    }

    @Test
    public void testGetTrackCount() {
        List<Track> tracks = contentApiDaoInterface.getTracks(0, 1000);
        int count = contentApiDaoInterface.getAllCount(Track.class);
        Assert.assertEquals(count, tracks.size());
    }

    @Test
    public void testGetAlbumCount() {
        List<Album> albums = contentApiDaoInterface.getAlbums(0, 1000);
        int count = contentApiDaoInterface.getAllCount(Album.class);
        Assert.assertEquals(count, albums.size());
    }

    @Test
    public void testGetArtistCount() {
        List<Artist> artists = contentApiDaoInterface.getArtists(0, 1000);
        int count = contentApiDaoInterface.getAllCount(Artist.class);
        Assert.assertEquals(count, artists.size());
    }


    @Test
    public void testGetAlbumInfo() {
        List<AlbumInfo> albumInfo = contentApiDaoInterface.getAlbumInfo(72);
        Assert.assertTrue(albumInfo.size() == 1);
        List<Album> albums = contentApiDaoInterface.getAlbums(0, 1000);
        Assert.assertEquals(albumInfo.get(0).getAlbumId(), albums.get(72).getId());
    }

    @Test
    public void testGetAlbumInfo1() {
        AlbumInfo albumInfo = contentApiDaoInterface.getAlbumInfo(72, ALBUM_NOTES);
        Assert.assertTrue(albumInfo != null);
        List<Album> albums = contentApiDaoInterface.getAlbums(0, 1000);
        Assert.assertEquals(albumInfo.getAlbumId(), albums.get(72).getId());
    }

    @Test
    public void testGetNonExistentAlbumInfo() {
        AlbumInfo albumInfo = contentApiDaoInterface.getAlbumInfo(-1, "");
        Assert.assertEquals(null, albumInfo);
    }

    @Test
    public void testGetArtistInfo() {
        List<ArtistInfo> artistInfo = contentApiDaoInterface.getArtistInfo(12);
        Assert.assertTrue(artistInfo.size() == 1);
        List<Artist> artists = contentApiDaoInterface.getArtists(0, 1000);
        Assert.assertEquals(artistInfo.get(0).getArtistId(), artists.get(12).getId());
    }

    @Test
    public void testGetArtistInfo1() {
        ArtistInfo artistInfo = contentApiDaoInterface.getArtistInfo(12, ARTIST_BIO);
        Assert.assertTrue(artistInfo != null);
        List<Artist> artists = contentApiDaoInterface.getArtists(0, 1000);
        Assert.assertEquals(artistInfo.getArtistId(), artists.get(12).getId());
    }

    @Test
    public void testGetTrackInfo() {
        List<ArtistInfo> artistInfo = contentApiDaoInterface.getArtistInfo(11);
        Assert.assertTrue(artistInfo.size() == 1);
        List<Artist> artists = contentApiDaoInterface.getArtists(0, 1000);
        Assert.assertEquals(artistInfo.get(0).getArtistId(), artists.get(11).getId());
    }

    @Test
    public void testGetNonExistentTrackInfo() {
        TrackInfo trackInfo = contentApiDaoInterface.getTrackInfo(-1);
        Assert.assertEquals(null, trackInfo);
    }
}
