/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.model.DeviceLinkCredentials;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.ResponseMatcher;
import org.springframework.xml.transform.StringSource;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.*;
import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.RequestCreators.withSoapEnvelope;
import static org.springframework.ws.test.server.ResponseMatchers.noFault;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MusicServiceConfig.class, MusicServiceEndpoint.class, OptionalMethodsEndpoint.class,
                                 ContentApiDefaultEndpoint.class, DeviceLinkAuthenticationModeEndpoint.class,
                                 MusicServiceGetExtendedMetadataEndpoint.class,
                                 PlaylistEditingEndpoint.class, StatusReportingEndpoint.class})
public class MusicServiceEndpointTests extends CredentialedApiTestsBase {
    protected static final String UNIMPLEMENTED_RESPONSE = "<ns:customFaultDetail xmlns:ns=\"" +
            MusicServiceConfig.SONOS_SOAP_NAMESPACE +
            "\"><ns:SonosError>1000</ns:SonosError>" +
            "<ns:ExceptionInfo>Method currently unimplemented: SaajSoapMessage {" +
            MusicServiceConfig.SONOS_SOAP_NAMESPACE +
            "}%s</ns:ExceptionInfo></ns:customFaultDetail>";
    private static final String GET_DEVICE_LINK_CODE_REQUEST =
            "<ns:getDeviceLinkCode xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">\n" +
                    "<ns:householdId>householdId653482</ns:householdId>\n" +
                    "</ns:getDeviceLinkCode>";
    private static final String GET_DEVICE_AUTH_TOKEN_BAD_CODE_REQUEST = "<ns:getDeviceAuthToken xmlns:ns=\"" +
            MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">\n" +
            "<ns:householdId>householdId653482</ns:householdId>\n" +
            "<ns:linkCode>1234567890</ns:linkCode>\n" +
            "<ns:linkDeviceId>09876543210987654321</ns:linkDeviceId>\n" +
            "</ns:getDeviceAuthToken>";
    private static final String GET_DEVICE_AUTH_TOKEN_CODE_REQUEST_FMT = "<ns:getDeviceAuthToken xmlns:ns=\"" +
            MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">\n" +
            "<ns:householdId>householdId653482</ns:householdId>\n" +
            "<ns:linkCode>%1$s</ns:linkCode>\n" +
            "<ns:linkDeviceId>%2$s</ns:linkDeviceId>\n" +
            "</ns:getDeviceAuthToken>";
    private static final String GET_DEVICE_AUTH_TOKEN_RETRY = "<SOAP-ENV:Fault xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "    <faultcode xmlns:ns0=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">ns0:Client.NOT_LINKED_RETRY</faultcode>\n" +
            "    <faultstring xml:lang=\"en\">NOT_LINKED_RETRY</faultstring>\n" +
            "    <detail>\n" +
            "        <ns:ExceptionInfo xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">NOT_LINKED_RETRY</ns:ExceptionInfo>\n" +
            "        <ns:SonosError xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">5</ns:SonosError>\n" +
            "    </detail>\n" +
            "</SOAP-ENV:Fault>";
    private static final String GET_DEVICE_AUTH_TOKEN_FAILURE = "<SOAP-ENV:Fault xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "    <faultcode xmlns:ns0=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">ns0:Client.NOT_LINKED_FAILURE</faultcode>\n" +
            "    <faultstring xml:lang=\"en\">NOT_LINKED_FAILURE</faultstring>\n" +
            "    <detail>\n" +
            "        <ns:ExceptionInfo xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">NOT_LINKED_FAILURE</ns:ExceptionInfo>\n" +
            "        <ns:SonosError xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">6</ns:SonosError>\n" +
            "    </detail>\n" +
            "</SOAP-ENV:Fault>";
    private static final Map<String, String> expectedRegUrlParts;
    static {
        Map<String, String> m = new HashMap<String, String>();
        m.put("protocol", "http");
        m.put("host", "localhost");
        m.put("port", "8080");
        m.put("path", "/deviceLink/home");
        m.put("query", "linkCode");
        expectedRegUrlParts = Collections.unmodifiableMap(m);
    }

    @Autowired
    private ApplicationContext applicationContext;

    private MockWebServiceClient mockClient;

    @Before
    public void createClient() {
        mockClient = MockWebServiceClient.createClient(applicationContext);
        authorizeWithService();
    }

    @After
    public void cleanUpAfterTest() {
        authToken = null;
        refreshToken = null;
    }

    @Test
    public void getSessionId() {
        testMethod(mockClient, buildHeader(), "getSessionId");
    }

    @Test
    public void getExtendedMetadataAlbum() {
        getExtendedMetadataHelper(ALBUM, "al:9");
    }

    @Test
    public void getExtendedMetadataArtist() {
        getExtendedMetadataHelper(ARTIST, "ar:7");
    }

    @Test
    public void getExtendedMetadataTrack() {
        getExtendedMetadataHelper(TRACK, "tr:13");
    }

    private void getExtendedMetadataHelper(String prefix, String id) {
        String payloadString = String.format(XML_PAYLOAD_WITH_ID, "getExtendedMetadata", id);
        String envelopeString = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), payloadString);
        Source envelopeSource = new StringSource(envelopeString);
        GetExtendedMetadataResponseMatcher matcher = new GetExtendedMetadataResponseMatcher(prefix);
        mockClient.sendRequest(withSoapEnvelope(envelopeSource)).andExpect(matcher);
    }

    @Test
    public void getMediaMetadataTrack() {
        String payloadString = String.format(XML_PAYLOAD_WITH_ID, "getMediaMetadata", "tr:13");
        String envelopeString = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), payloadString);
        Source envelopeSource = new StringSource(envelopeString);
        GetMediaMetadataResponseMatcher matcher = new GetMediaMetadataResponseMatcher();
        mockClient.sendRequest(withSoapEnvelope(envelopeSource)).andExpect(matcher);
    }

    @Test
    public void getMediaUri() {
        String payloadString = String.format(XML_PAYLOAD_WITH_ID, "getMediaURI", "tr:13");
        String envelopeString = String.format(SOAP_ENVELOPE_TEMPLATE, buildHeader(), payloadString);
        Source envelopeSource = new StringSource(envelopeString);

        GetMediaUriResponseMatcher matcher = new GetMediaUriResponseMatcher();
        mockClient.sendRequest(withSoapEnvelope(envelopeSource)).andExpect(matcher);
    }

    @Test
    public void getLastUpdate() {
        Source envelope = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                buildHeader(), String.format(EMPTY_XML_PAYLOAD, "getLastUpdate")));
        Source expectedResponsePayload = new StringSource(
                "<ns2:getLastUpdateResponse xmlns:ns2=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">" +
                "    <ns2:getLastUpdateResult>" +
                "        <ns2:catalog>12345</ns2:catalog>" +
                "        <ns2:favorites>23456</ns2:favorites>" +
                "        <ns2:pollInterval>" + Integer.toString(MusicServiceConfig.getPollInterval()) + "</ns2:pollInterval>" +
                "    </ns2:getLastUpdateResult>" +
                "</ns2:getLastUpdateResponse>");

        mockClient.sendRequest(withSoapEnvelope(envelope)).andExpect(payload(expectedResponsePayload));
    }

    @Test
    public void optionalMethodsEndpoint() {
        final String methods[] = {"createItem", "deleteItem", "getScrollIndices", "rateItem"};
        testUnimplementedMethod(mockClient, buildHeader(), methods);
    }

    @Test
    public void deviceLinkAuthenticationModeEndpoint() {
        Source getLinkCodeRequest = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                String.format(HEADER_CREDENTIALS, ""), GET_DEVICE_LINK_CODE_REQUEST));
        LinkCodeData linkCodeData;
        GetDeviceLinkCodeResponseMatcher linkCodeResponseMatcher = new GetDeviceLinkCodeResponseMatcher();
        GetDeviceAuthTokenResponseMatcher authTokenResponseMatcher = new GetDeviceAuthTokenResponseMatcher();
        mockClient.sendRequest(withSoapEnvelope(getLinkCodeRequest)).andExpect(noFault()).andExpect(linkCodeResponseMatcher);
        linkCodeData = linkCodeResponseMatcher.linkCodeData;

        mockClient.sendRequest(withSoapEnvelope(new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                String.format(HEADER_CREDENTIALS, ""), GET_DEVICE_AUTH_TOKEN_BAD_CODE_REQUEST)))).
                andExpect(payload(new StringSource(GET_DEVICE_AUTH_TOKEN_FAILURE)));

        String emptyGetTokenRequest = String.format(GET_DEVICE_AUTH_TOKEN_CODE_REQUEST_FMT, "", "");
        String goodGetTokenRequest = String.format(GET_DEVICE_AUTH_TOKEN_CODE_REQUEST_FMT,
                linkCodeData.linkCode,
                linkCodeData.linkDeviceId);

        mockClient.sendRequest(withSoapEnvelope(new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                String.format(HEADER_CREDENTIALS, ""), emptyGetTokenRequest)))).
                andExpect(payload(new StringSource(GET_DEVICE_AUTH_TOKEN_RETRY)));

        mockClient.sendRequest(withSoapEnvelope(new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                String.format(HEADER_CREDENTIALS, ""), goodGetTokenRequest)))).
                andExpect(payload(new StringSource(GET_DEVICE_AUTH_TOKEN_RETRY)));

        DeviceLinkCredentials deviceLinkCredentials = DeviceLinkCredentials.lookupDeviceLinkCredentialsByLinkCode(linkCodeData.linkCode);
        assert deviceLinkCredentials != null;

        // fake the login process
        final String fakeAuthToken = "fake auth token";
        final String fakeAuthKey = "fake auth key";
        deviceLinkCredentials.setAuthToken(fakeAuthToken);
        deviceLinkCredentials.setAuthKey(fakeAuthKey);
        DeviceLinkCredentials.storeDeviceLinkCredentialsByLinkCode(linkCodeData.linkCode, deviceLinkCredentials);

        authTokenResponseMatcher.linkCodeData = linkCodeData;
        mockClient.sendRequest(withSoapEnvelope(new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                String.format(HEADER_CREDENTIALS, ""), goodGetTokenRequest)))).
                andExpect(authTokenResponseMatcher);

        assert linkCodeData.authToken.equals(fakeAuthToken);
        assert linkCodeData.privateKey.equals(fakeAuthKey);
    }

    @Test
    public void playlistEditingEndpoint() {
        final String methods[] = {"createContainer", "deleteContainer", "renameContainer", "addToContainer", "reorderContainer", "removeFromContainer"};
        String header = buildHeader();
        testUnimplementedMethod(mockClient, header, methods);
    }

    @Test
    public void statusReportingEndpoint() {
        final String methods[] = {"setPlayedSeconds", "reportAccountAction", "reportPlaySeconds", "reportPlayStatus", "reportStatus"};
        testUnimplementedMethod(mockClient, buildHeader(), methods);
    }

    public static void testMethod(MockWebServiceClient client, String header, String method) {
        Source envelope = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                header, String.format(EMPTY_XML_PAYLOAD, method)));
        Source expectedResponsePayload = new StringSource(String.format(EMPTY_XML_PAYLOAD, method + "Response"));

        client.sendRequest(withSoapEnvelope(envelope)).andExpect(payload(expectedResponsePayload));
    }

    class LinkCodeData {
        String linkCode;
        String regUrl;
        String linkDeviceId;
        boolean showLinkCode;
        String authToken;
        String privateKey;
    }

    class GetDeviceLinkCodeResponseMatcher implements ResponseMatcher {
        LinkCodeData linkCodeData = new LinkCodeData();

        @Override
        public void match(WebServiceMessage request,
                          WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            getDataFromLinkCodeResponse(src, linkCodeData);
        }
    }

    class GetDeviceAuthTokenResponseMatcher implements ResponseMatcher {
        LinkCodeData linkCodeData;

        @Override
        public void match(WebServiceMessage request,
                          WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            getDataFromAuthTokenResponse(src, linkCodeData);
        }
    }

    class GetMediaUriResponseMatcher implements ResponseMatcher {
        @Override
        public void match(WebServiceMessage request, WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            basicResponseChecks(src, "getMediaURIResponse", "getMediaURIResult", 1);
        }
    }

    class GetMediaMetadataResponseMatcher implements ResponseMatcher {
        @Override
        public void match(WebServiceMessage request, WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            basicResponseChecks(src, "getMediaMetadataResponse", "getMediaMetadataResult", 7);
        }
    }

    class GetExtendedMetadataResponseMatcher implements ResponseMatcher {
        String firstChildType;
        GetExtendedMetadataResponseMatcher(String prefix) {
            switch (prefix) {
                case ALBUM:
                case ARTIST:
                    firstChildType = "mediaCollection";
                    break;
                case TRACK:
                    firstChildType = "mediaMetadata";
                    break;
                default:
                    assert false;
            }
        }

        @Override
        public void match(WebServiceMessage request, WebServiceMessage response) throws IOException, AssertionError {
            Source src = response.getPayloadSource();
            // send -1 for "expected number of children" to suppress that check. number of children in extendedMetadata varies.
            NodeList extendedMetadataChildren = basicResponseChecks(src, "getExtendedMetadataResponse", "getExtendedMetadataResult", -1);
            assert extendedMetadataChildren.getLength() >= 1;
            assert extendedMetadataChildren.item(0).getLocalName().equals(firstChildType);
        }
    }

    static NodeList basicResponseChecks(Source payloadSource, String responseName, String resultName, int numOfChildren) {
        assert payloadSource != null;
        assert payloadSource instanceof DOMSource;

        DOMSource domSource = (DOMSource)payloadSource;
        Node responseNode = domSource.getNode();
        assert responseNode.getLocalName().equals(responseName);

        Node resultNode = responseNode.getFirstChild();
        assert resultNode != null;
        assert resultNode.getLocalName().equals(resultName);

        NodeList children = resultNode.getChildNodes();
        if (numOfChildren > -1) {
            assert children.getLength() == numOfChildren;
        }

        return children;
    }

    private void getDataFromLinkCodeResponse(Source payloadSource, LinkCodeData linkCodeData){
        NodeList children = basicResponseChecks(payloadSource, "getDeviceLinkCodeResponse", "getDeviceLinkCodeResult", 4);

        for (int i = 0; i < 4; i++) {
            Node child = children.item(i);
            String name = child.getLocalName();
            switch(name) {
                case "regUrl":
                    linkCodeData.regUrl = child.getFirstChild().getTextContent();
                    URL parsedUrl;
                    try {
                        parsedUrl = new URL(linkCodeData.regUrl);
                    } catch (MalformedURLException e) {
                        assert false;
                        return;
                    }

                    assert parsedUrl.getProtocol().equals(expectedRegUrlParts.get("protocol"));
                    assert parsedUrl.getHost().equals(expectedRegUrlParts.get("host"));
                    assert Integer.toString(parsedUrl.getPort()).equals(expectedRegUrlParts.get("port"));
                    assert parsedUrl.getPath().equals(expectedRegUrlParts.get("path"));
                    assert parsedUrl.getQuery().startsWith(expectedRegUrlParts.get("query") + "=");

                    String query = parsedUrl.getQuery();
                    int queryLength = (expectedRegUrlParts.get("query") + "=").length();
                    assert query.length() == queryLength + 10;

                    if (linkCodeData.linkCode == null) {
                        linkCodeData.linkCode = query.substring(queryLength);
                    } else {
                        assert query.substring(queryLength).equals(linkCodeData.linkCode);
                    }
                    break;
                case "linkCode":
                    if (linkCodeData.linkCode == null) {
                        linkCodeData.linkCode = child.getFirstChild().getTextContent();
                        assert linkCodeData.linkCode.length() == 10;
                    } else {
                        assert child.getFirstChild().getTextContent().equals(linkCodeData.linkCode);
                    }
                    break;
                case "showLinkCode":
                    linkCodeData.showLinkCode = child.getFirstChild().getTextContent().equals("true");
                    break;
                case "linkDeviceId":
                    linkCodeData.linkDeviceId = child.getFirstChild().getTextContent();
                    assert linkCodeData.linkDeviceId.length() == 20;
                    break;
                default:
                    assert false;
            }
        }

        assert linkCodeData.linkCode != null;
        assert linkCodeData.linkDeviceId != null;
        assert linkCodeData.regUrl != null;
    }

    private void getDataFromAuthTokenResponse(Source payloadSource, LinkCodeData linkCodeData) {
        NodeList children = basicResponseChecks(payloadSource, "getDeviceAuthTokenResponse", "getDeviceAuthTokenResult", 2);

        for (int i = 0; i < 2; i++) {
            Node child = children.item(i);
            String name = child.getLocalName();
            switch (name) {
                case "authToken":
                    linkCodeData.authToken = child.getFirstChild().getTextContent();
                    break;
                case "privateKey":
                    linkCodeData.privateKey = child.getFirstChild().getTextContent();
                    break;
                default:
                    assert false;
            }
        }

        assert linkCodeData.authToken != null;
        assert linkCodeData.privateKey != null;
    }
}