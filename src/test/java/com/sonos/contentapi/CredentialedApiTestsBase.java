/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.model.DeviceLinkCredentials;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.Source;

import static org.springframework.ws.test.server.RequestCreators.withSoapEnvelope;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

public class CredentialedApiTestsBase {

    protected static final String SOAP_ENVELOPE_TEMPLATE = "<soapenv:Envelope " +
            "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
            "xmlns:gs=\"http://www.sonos.com/Services/1.1\">" +
            "<soapenv:Header>%1$s</soapenv:Header>" +
            "<soapenv:Body>%2$s</soapenv:Body>" +
            "</soapenv:Envelope>";
    protected static final String HEADER_CREDENTIALS = "<ns:credentials xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">" +
            "<ns:deviceId>00-11-22-33-44-55:0</ns:deviceId>" +
            "<ns:deviceProvider>Sonos</ns:deviceProvider>" +
            "%s" + // loginToken if any
            "</ns:credentials>";
    protected static final String LOGIN_TOKEN = "<ns:loginToken>" +
            "<ns:token>%1$s</ns:token>" +
            "<ns:key>%2$s</ns:key>" +
            "<ns:householdId>12345</ns:householdId>" +
            "</ns:loginToken>";
    protected static final String EMPTY_XML_PAYLOAD = "<ns:%s xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\"/>";
    protected static final String XML_PAYLOAD_WITH_ID = "<ns:%1$s xmlns:ns=\"" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "\">" +
            "<ns:id>%2$s</ns:id>" +
            "</ns:%1$s>";
    protected String authToken;
    protected String refreshToken;

    public static void testUnimplementedMethod(MockWebServiceClient client, String header, String[] methods) {
        for(String method : methods) {
            Source envelope = new StringSource(String.format(SOAP_ENVELOPE_TEMPLATE,
                    header, String.format(EMPTY_XML_PAYLOAD, method)));
            Source expectedResponsePayload = new StringSource(String.format(MusicServiceEndpointTests.UNIMPLEMENTED_RESPONSE, method));

            client.sendRequest(withSoapEnvelope(envelope)).andExpect(payload(expectedResponsePayload));
        }
    }

    protected void authorizeWithService() {
        DeviceLinkCredentials cred = new DeviceLinkCredentials("link code", "12345", DeviceLinkCredentials.generateAuthToken(), "refresh token!");
        DeviceLinkCredentials.storeDeviceLinkCredentialsByHouseholdId(cred);
        authToken = cred.getAuthToken();
        refreshToken = cred.getAuthKey();
    }

    protected String buildHeader() {
        String loginToken = String.format(MusicServiceEndpointTests.LOGIN_TOKEN, authToken, refreshToken);
        return String.format(MusicServiceEndpointTests.HEADER_CREDENTIALS, loginToken);
    }
}
