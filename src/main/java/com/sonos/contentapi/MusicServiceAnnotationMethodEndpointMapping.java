/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping;
import org.springframework.xml.namespace.SimpleNamespaceContext;
import org.w3c.dom.Node;

import javax.annotation.PostConstruct;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>This class extends the PayloadRootAnnotationMethodEndpointMapping class to allow finer grain routing of SOAP
 * requests to handlers. In addition to the namespace and local parts that PayloadRoot allows, this class also allows
 * routing based on the contents of the id field of incoming messages. The routing can be controlled by absolute
 * matching of the ID or prefix matching.</p>
 *
 * <p>Methods within endpoint are annotated with @Payload root, just as for the
 * PayloadRootAnnotationMethodEndpoinMapping. However, by adding the @BindObjectId annotation, routing can be controlled
 * further by listing either an absolute or a prefix. The following code is an example of both absolute and prefix based
 * routing:</p>
 *
 * <code>
   @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
   @ResponsePayload
   @BindObjectId("root")
   public GetMetadataResponse getMetadataRoot(@RequestPayload GetMetadata request) { ... }

   @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
   @ResponsePayload
   @BindObjectId("ar:")
   public GetMetadataResponse getMetadataPrefixArtist(@RequestPayload GetMetadata request) { ... }
   </code>
 *
 * <p>For both absolute and prefix matching the first step in routing is to extract the ID from the incoming message.
 * This step is accomplished by using XPath expressions to read the id field of the XML. Once read, the id is matched to
 * a regular expression containing all the possible prefixes. If no match is found, then the id is assumed to be an
 * absolute match. In any case, either the prefix or absolute id are appended to the QName from the incoming XML message
 * to form a key to lookup the proper endpoint method. If no method is found, then an exception is thrown.</p>
 */
public class MusicServiceAnnotationMethodEndpointMapping extends PayloadRootAnnotationMethodEndpointMapping {
    private static final Logger logger = LoggerFactory.getLogger(MusicServiceAnnotationMethodEndpointMapping.class);
    private static final String SEPARATOR = ":";

    private SimpleNamespaceContext namespaceContext = new SimpleNamespaceContext();
    private XPath xpath = XPathFactory.newInstance().newXPath();
    private Pattern prefixPattern = null;
    private Set<String> prefixes = new HashSet<>();

    @PostConstruct
    public void init() {
        xpath.setNamespaceContext(namespaceContext);
    }

    /**
     * This helper method matches an id against the regular expression that recognizes all prefixes and returns the
     * prefix.
     *
     * @param id - This parameter has the object id from the request.
     * @return The object returned is a string containing the object id prefix.
     */
    private String _findPrefix(String id) {
        if(prefixPattern == null) {
            // Create the regex by joining all the prefixes. The '^' character anchors the prefix to the beginning
            // of the object id
            String regex = "(^" + String.join(")|(^", prefixes) + ")";
            prefixPattern = Pattern.compile(regex);
        }

        // Return either the prefix, stored in group 0 of the matcher, or null if a prefix was not found
        Matcher prefixMatcher = prefixPattern.matcher(id);
        return prefixMatcher.find() ? prefixMatcher.group(0) : null;
    }

    @Override
    protected QName getLookupKeyForMessage(MessageContext context) throws Exception {
        Node root = ((DOMSource)context.getRequest().getPayloadSource()).getNode();
        if (root.getPrefix() != null) {
            namespaceContext.bindNamespaceUri(root.getPrefix(), root.getNamespaceURI());
        } else {
            namespaceContext.bindDefaultNamespaceUri(root.getNamespaceURI());
        }

        QName qName = super.getLookupKeyForMessage(context);
        String prefix = root.getPrefix() == null ? "" : root.getPrefix();

        // Extract the id field from the XML SOAP request
        String id = xpath.evaluate("//" + prefix + ":id/text()", root);
        if(id.length() > 0) {
            String suffix = _findPrefix(id);
            qName = new QName(qName.getNamespaceURI(), qName.getLocalPart() + SEPARATOR + (suffix != null ? suffix : id));
        }

        return qName;
    }

    @Override
    protected List<QName> getLookupKeysForMethod(Method method) {
        List<QName> qNames = super.getLookupKeysForMethod(method);

        Annotation[] annotations = method.getDeclaredAnnotations();
        for(Annotation annotation : annotations) {
            if(annotation.annotationType().equals(BindObjectId.class)) {
                BindObjectId boundObjectId = (BindObjectId)annotation;
                for(ListIterator<QName> iter = qNames.listIterator(); iter.hasNext();) {
                    QName qName = iter.next();

                    String suffix = boundObjectId.value();
                    // If for some reason someone puts both a value and a prefix on the @BindObjectId annotation, then
                    // annotation value (or value()) should override the prefix value
                    if(boundObjectId.value().length() <= 0 && boundObjectId.prefix().length() > 0) {
                        suffix = boundObjectId.prefix();
                        prefixes.add(boundObjectId.prefix());
                    }

                    // Append the objectId value or prefix to the original localPart of the XML SOAP request
                    String boundLocalPart = qName.getLocalPart() + SEPARATOR + suffix;
                    iter.set(new QName(qName.getNamespaceURI(), boundLocalPart));
                }
            }
        }

        return qNames;
    }
}
