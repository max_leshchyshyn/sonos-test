/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Track;
import com.sonos.contentapi.util.MusicLibraryDataUtil;
import com.sonos.services._1.AbstractMedia;
import com.sonos.services._1.CustomFaultDetail;
import com.sonos.services._1.GetMetadata;
import com.sonos.services._1.GetMetadataResponse;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeader;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.sonos.contentapi.model.enums.ObjectId.*;
import static com.sonos.contentapi.model.enums.ObjectIdPrefix.*;
import static com.sonos.contentapi.util.MusicLibraryDataUtil.buildArtistMediaCollection;
import static com.sonos.contentapi.util.media.builders.MediaCollectionBuilder.buildMediaCollection;

@Endpoint
public class MusicServiceGetMetadataEndpoint extends ContentApiDefaultEndpoint {
    @Autowired
    MessageSource source;

    private static final Logger logger = LoggerFactory.getLogger(MusicServiceEndpoint.class);

    private Collection<AbstractMedia> populateTrackCollectionForResponse(List<Track> tracks) {
        return tracks.stream().map(track -> MusicLibraryDataUtil.buildTrackMediaMetadata(track, source, getBaseRequestUrl())).collect(Collectors.toList());
    }

    private Collection<AbstractMedia> populateAlbumCollectionForResponse(List<Album> albums) {
        String baseRequestUrl = getBaseRequestUrl();
        return albums.stream().map(album -> MusicLibraryDataUtil.buildAlbumMediaCollection(album, source, getBaseRequestUrl())).collect(Collectors.toList());
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @ResponsePayload
    public CustomFaultDetail unknownGetMetadataRoute(SoapHeader requestHeader, MessageContext messageContext) {
        logger.info("GetMetadata");

        return _defaultResponse(requestHeader, messageContext);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(ROOT)
    @ResponsePayload
    public GetMetadataResponse getMetadataRoot(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataRoot");

        _verifyObjectId(ROOT, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaList = responseList.getMediaCollectionOrMediaMetadata();

        mediaList.add(buildMediaCollection(source).id(BROWSE_ALBUMS).title(BROWSE_ALBUMS).itemType(ItemType.ALBUM).build());
        mediaList.add(buildMediaCollection(source).id(BROWSE_ARTISTS).title(BROWSE_ARTISTS).itemType(ItemType.ARTIST).build());
        mediaList.add(buildMediaCollection(source).id(BROWSE_TRACKS).title(BROWSE_TRACKS).itemType(ItemType.CONTAINER).build());

        responseList.setIndex(request.getIndex());
        responseList.setTotal(mediaList.size());
        responseList.setCount(mediaList.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_ALBUMS)
    @ResponsePayload
    public GetMetadataResponse getMetadataBrowseAlbums(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataBrowseAlbums");

        _verifyObjectId(BROWSE_ALBUMS, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<Album> albums = dataSource.getAlbums(request.getIndex(), request.getCount());
        mediaCollection.addAll(populateAlbumCollectionForResponse(albums));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(dataSource.getAllCount(Album.class));
        responseList.setCount(mediaCollection.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_ALBUMS_V2)
    @ResponsePayload
    public GetMetadataResponse getMetadataBrowseAlbums_V2(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataBrowseAlbums_V2");

        _verifyObjectId(BROWSE_ALBUMS_V2, request);

        return new GetMetadataResponse();
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_ARTISTS)
    @ResponsePayload
    public GetMetadataResponse getMetadataBrowseArtists(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataBrowseArtists");

        _verifyObjectId(BROWSE_ARTISTS, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<Artist> artists = dataSource.getArtists(request.getIndex(), request.getCount());
        String baseRequestUrl = getBaseRequestUrl();
        mediaCollection.addAll(artists.stream()
                .map(artist -> buildArtistMediaCollection(artist, source, getBaseRequestUrl()))
                .collect(Collectors.toList()));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(dataSource.getAllCount(Artist.class));
        responseList.setCount(mediaCollection.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_RADIO)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseRadio(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseRadio");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_TRACKS)
    @ResponsePayload
    public GetMetadataResponse getMetadataBrowseTracks(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataBrowseTracks");

        _verifyObjectId(BROWSE_TRACKS, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<Track> tracks = dataSource.getTracks(request.getIndex(), request.getCount());
        mediaCollection.addAll(populateTrackCollectionForResponse(tracks));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(dataSource.getAllCount(Track.class));
        responseList.setCount(mediaCollection.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_TOTM)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseTOTM(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseTOTM");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_LISTEN_NOW)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseListenNow(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseListenNow");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_PLAYLISTS)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowsePlaylists(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowsePlaylists");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_FAVORITES)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseFavorites(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseFavorites");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_FAVORITE_TRACKS)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseFavoriteTracks(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseFavoriteTracks");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_FAVORITE_ALBUMS)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseFavoriteAlbums(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseFavoriteAlbums");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(BROWSE_CONCIERGE)
    @ResponsePayload
    public CustomFaultDetail getMetadataBrowseConcierge(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataBrowseConcierge");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(CONCIERGE_GENRE)
    @ResponsePayload
    public CustomFaultDetail getMetadataConciergeGenre(SoapHeader header, MessageContext context) {
        logger.info("GetMetadataConciergeGenre");

        return _defaultResponse(header, context);
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(prefix = ALBUM)
    @ResponsePayload
    public GetMetadataResponse getMetadataPrefixAlbum(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataPrefixAlbum");

        _verifyObjectIdPrefix(ALBUM, request);
        int albumId = parseIdFromRequest(ALBUM, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<Track> tracks = dataSource.getAlbumTracks(albumId, request.getIndex(), request.getCount());
        mediaCollection.addAll(populateTrackCollectionForResponse(tracks));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(dataSource.getCount(Track.class, "album.id", albumId));
        responseList.setCount(mediaCollection.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(prefix = ARTIST)
    @ResponsePayload
    public GetMetadataResponse getMetadataPrefixArtist(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataPrefixArtist");

        _verifyObjectIdPrefix(ARTIST, request);
        int artistId = parseIdFromRequest(ARTIST, request);

        MediaList responseList = new MediaList();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<Album> albums = dataSource.getArtistAlbums(artistId, request.getIndex(), request.getCount());
        mediaCollection.addAll(populateAlbumCollectionForResponse(albums));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(dataSource.getCount(Album.class, "artist.id", artistId));
        responseList.setCount(mediaCollection.size());

        return new GetMetadataResponse() {{
            setGetMetadataResult(responseList);
        }};
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(prefix = PLAYLIST)
    @ResponsePayload
    public GetMetadataResponse getMetadataPrefixPlaylist(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataPrefixPlaylist");

        _verifyObjectIdPrefix(PLAYLIST, request);

        return new GetMetadataResponse();
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(prefix = PROGRAM_RADIO)
    @ResponsePayload
    public GetMetadataResponse getMetadataPrefixProgramRadio(@RequestPayload GetMetadata request) {
        logger.info("GetMetadataPrefixProgramRadio");

        _verifyObjectIdPrefix(PROGRAM_RADIO, request);

        return new GetMetadataResponse();
    }
}
