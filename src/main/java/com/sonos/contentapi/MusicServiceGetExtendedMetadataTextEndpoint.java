/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.AlbumInfo;
import com.sonos.contentapi.model.db.ArtistInfo;
import com.sonos.services._1.GetExtendedMetadataText;
import com.sonos.services._1.GetExtendedMetadataTextResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.ALBUM;
import static com.sonos.contentapi.model.enums.ObjectIdPrefix.ARTIST;

@Endpoint
public class MusicServiceGetExtendedMetadataTextEndpoint extends ContentApiDefaultEndpoint {
    @Autowired
    MessageSource source;

    private static final Logger logger = LoggerFactory.getLogger(MusicServiceEndpoint.class);

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getExtendedMetadataText")
    @BindObjectId(prefix = ARTIST)
    @ResponsePayload
    public GetExtendedMetadataTextResponse getExtendedMetadataTextArtist(@RequestPayload GetExtendedMetadataText request) {
        logger.info("getExtendedMetadataTextArtist");

        _verifyObjectIdPrefix(ARTIST, request);
        int artistId = parseIdFromRequest(ARTIST, request);

        GetExtendedMetadataTextResponse response = new GetExtendedMetadataTextResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        ArtistInfo artistInfo = dataSource.getArtistInfo(artistId, request.getType());
        response.setGetExtendedMetadataTextResult(artistInfo.getDescription());

        return response;
    }

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getExtendedMetadataText")
    @BindObjectId(prefix = ALBUM)
    @ResponsePayload
    public GetExtendedMetadataTextResponse getExtendedMetadataTextAlbum(@RequestPayload GetExtendedMetadataText request) {
        logger.info("getExtendedMetadataTextAlbum");

        _verifyObjectIdPrefix(ALBUM, request);
        int albumId = parseIdFromRequest(ALBUM, request);

        GetExtendedMetadataTextResponse response = new GetExtendedMetadataTextResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        AlbumInfo albumInfo = dataSource.getAlbumInfo(albumId, request.getType());
        response.setGetExtendedMetadataTextResult(albumInfo.getDescription());

        return response;
    }
}
