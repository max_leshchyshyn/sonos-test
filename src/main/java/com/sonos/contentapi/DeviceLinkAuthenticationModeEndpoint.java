/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.model.DeviceLinkTokenFailureException;
import com.sonos.contentapi.model.DeviceLinkTokenRetryException;
import com.sonos.contentapi.model.DeviceLinkCredentials;
import com.sonos.services._1.DeviceAuthTokenResult;
import com.sonos.services._1.DeviceLinkCodeResult;
import com.sonos.services._1.GetDeviceAuthToken;
import com.sonos.services._1.GetDeviceAuthTokenResponse;
import com.sonos.services._1.GetDeviceLinkCode;
import com.sonos.services._1.GetDeviceLinkCodeResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class DeviceLinkAuthenticationModeEndpoint extends ContentApiDefaultEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(DeviceLinkAuthenticationModeEndpoint.class);
    private static final int LINK_CODE_LENGTH = 10;
    private static final int LINK_DEVICE_ID_LENGTH = 20;

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getDeviceLinkCode")
    @ResponsePayload
    public GetDeviceLinkCodeResponse getDeviceLinkCode(@RequestPayload GetDeviceLinkCode getDeviceLinkCode) {

        String url = getBaseRequestUrl() + "/deviceLink/home";

        DeviceLinkCodeResult result = new DeviceLinkCodeResult();
        GetDeviceLinkCodeResponse response = new GetDeviceLinkCodeResponse();
        String linkCode = generateLinkCode();
        String linkDeviceId = generateLinkDeviceId();

        DeviceLinkCredentials deviceLinkCredentials = new DeviceLinkCredentials();
        deviceLinkCredentials.setHouseholdId(getDeviceLinkCode.getHouseholdId());
        deviceLinkCredentials.setLinkDeviceId(linkDeviceId);

        // Associate a DeviceLinkCredentials object with a link code, setting up for the second step of the linking process
        DeviceLinkCredentials.storeDeviceLinkCredentialsByLinkCode(linkCode, deviceLinkCredentials);

        result.setRegUrl(url + "?linkCode=" + linkCode);
        result.setLinkCode(linkCode);
        result.setLinkDeviceId(linkDeviceId);
        result.setShowLinkCode(true);
        response.setGetDeviceLinkCodeResult(result);

        return response;
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getDeviceAuthToken")
    @ResponsePayload
    public GetDeviceAuthTokenResponse getDeviceAuthToken(@RequestPayload GetDeviceAuthToken getDeviceAuthToken)
            throws DeviceLinkTokenRetryException, DeviceLinkTokenFailureException {
        if (getDeviceAuthToken.getLinkCode() == null || getDeviceAuthToken.getLinkCode().length() == 0) {
            throw new DeviceLinkTokenRetryException();
        }
        DeviceLinkCredentials credentials =
                DeviceLinkCredentials.lookupDeviceLinkCredentialsByLinkCode(getDeviceAuthToken.getLinkCode());
        if (credentials == null || !credentials.getLinkDeviceId().equals(getDeviceAuthToken.getLinkDeviceId())) {
            DeviceLinkCredentials.removeLinkCodeMapping(getDeviceAuthToken.getLinkCode());
            throw new DeviceLinkTokenFailureException();
        }

        String authToken = credentials.getAuthToken();
        if (authToken != null) {
            GetDeviceAuthTokenResponse response = new GetDeviceAuthTokenResponse();
            DeviceAuthTokenResult result = new DeviceAuthTokenResult();
            result.setAuthToken(authToken);
            result.setPrivateKey(credentials.getAuthKey());

            // link codes should generally be short-lived, although here we do not implement any timeout,
            // for conciseness' sake. Once the user has successfully linked their account, erase the link code
            // mapping, but retain the auth code/refresh token/household ID tuple so we can check it later
            DeviceLinkCredentials.removeLinkCodeMapping(getDeviceAuthToken.getLinkCode());
            DeviceLinkCredentials.storeDeviceLinkCredentialsByHouseholdId(credentials);

            response.setGetDeviceAuthTokenResult(result);
            return response;
        }

        throw new DeviceLinkTokenRetryException();
    }

    private String generateLinkCode() {
        return RandomStringUtils.randomAlphanumeric(LINK_CODE_LENGTH).toUpperCase();
    }

    private String generateLinkDeviceId() {
        return RandomStringUtils.randomAlphanumeric(LINK_DEVICE_ID_LENGTH).toUpperCase();
    }
}
