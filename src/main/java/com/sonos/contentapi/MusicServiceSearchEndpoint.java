/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.services._1.AbstractMedia;
import com.sonos.services._1.GetMetadata;
import com.sonos.services._1.GetMetadataResponse;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaList;
import com.sonos.services._1.Search;
import com.sonos.services._1.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.sonos.contentapi.model.enums.ObjectId.SEARCH;
import static com.sonos.contentapi.model.enums.SearchId.*;
import static com.sonos.contentapi.util.MusicLibraryDataUtil.buildAlbumMediaCollection;
import static com.sonos.contentapi.util.MusicLibraryDataUtil.buildArtistMediaCollection;
import static com.sonos.contentapi.util.MusicLibraryDataUtil.buildTrackMediaMetadata;
import static com.sonos.contentapi.util.media.builders.MediaCollectionBuilder.buildMediaCollection;

@Endpoint
public class MusicServiceSearchEndpoint extends ContentApiDefaultEndpoint {
    @Autowired
    MessageSource source;

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMetadata")
    @BindObjectId(SEARCH)
    @ResponsePayload
    public GetMetadataResponse getMetadataSearch(@RequestPayload GetMetadata request) {
        _verifyObjectId(SEARCH, request);
        GetMetadataResponse response = new GetMetadataResponse();
        MediaList responseList = new MediaList();

        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();
        mediaCollection.addAll(Arrays.stream(SEARCH_IDS)
                .map(type -> buildMediaCollection(source)
                        .id(type)
                        .itemType(ItemType.SEARCH)
                        .title(type.substring(0, 1).toUpperCase() + type.substring(1))
                        .build())
                .limit(request.getCount())
                .collect(Collectors.toList()));

        responseList.setIndex(request.getIndex());
        responseList.setTotal(SEARCH_IDS.length);
        responseList.setCount(mediaCollection.size());

        response.setGetMetadataResult(responseList);
        return response;
    }

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "search")
    @BindObjectId(ARTISTS)
    @ResponsePayload
    public SearchResponse searchArtists(@RequestPayload Search request) {
        _verifyObjectId(ARTISTS, request);
        SearchResponse response = new SearchResponse();
        MediaList responseList = new MediaList();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();
        mediaCollection.addAll(dataSource.getSearchArtists(ARTISTS, request.getTerm(), request.getIndex(), request.getCount()).stream()
                .map(artist -> buildArtistMediaCollection(artist, source, getBaseRequestUrl()))
                .collect(Collectors.toList()));

        responseList.setIndex(request.getIndex());
        responseList.setCount(mediaCollection.size());
        responseList.setTotal(mediaCollection.size());

        response.setSearchResult(responseList);
        return response;
    }

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "search")
    @BindObjectId(ALBUMS)
    @ResponsePayload
    public SearchResponse searchAlbums(@RequestPayload Search request) {
        _verifyObjectId(ALBUMS, request);
        SearchResponse response = new SearchResponse();
        MediaList responseList = new MediaList();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();
        mediaCollection.addAll(dataSource.getSearchAlbums(ALBUMS, request.getTerm(), request.getIndex(), request.getCount()).stream()
                .map(album -> buildAlbumMediaCollection(album, source, getBaseRequestUrl()))
                .collect(Collectors.toList()));

        responseList.setIndex(request.getIndex());
        responseList.setCount(mediaCollection.size());
        responseList.setTotal(mediaCollection.size());

        response.setSearchResult(responseList);
        return response;
    }

    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "search")
    @BindObjectId(TRACKS)
    @ResponsePayload
    public SearchResponse searchTracks(@RequestPayload Search request) {
        _verifyObjectId(TRACKS, request);
        SearchResponse response = new SearchResponse();
        MediaList responseList = new MediaList();

        ContentApiDaoInterface dataSource = new ContentApiDao();
        List<AbstractMedia> mediaCollection = responseList.getMediaCollectionOrMediaMetadata();
        mediaCollection.addAll(dataSource.getSearchTracks(TRACKS, request.getTerm(), request.getIndex(), request.getCount()).stream()
                .map(track -> buildTrackMediaMetadata(track, source, getBaseRequestUrl()))
                .collect(Collectors.toList()));

        responseList.setIndex(request.getIndex());
        responseList.setCount(mediaCollection.size());
        responseList.setTotal(mediaCollection.size());

        response.setSearchResult(responseList);
        return response;
    }
}
