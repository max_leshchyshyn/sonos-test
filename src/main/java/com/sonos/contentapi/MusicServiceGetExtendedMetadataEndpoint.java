/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Track;
import com.sonos.contentapi.util.MusicLibraryDataUtil;
import com.sonos.services._1.ExtendedMetadata;
import com.sonos.services._1.GetExtendedMetadata;
import com.sonos.services._1.GetExtendedMetadataResponse;
import com.sonos.services._1.RelatedText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.*;
import static com.sonos.contentapi.util.media.builders.RelatedTextBuilder.buildRelatedText;

@Endpoint
public class MusicServiceGetExtendedMetadataEndpoint extends ContentApiDefaultEndpoint {
    @Autowired
    MessageSource source;

    private static final Logger logger = LoggerFactory.getLogger(MusicServiceEndpoint.class);

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getExtendedMetadata")
    @BindObjectId(prefix = ALBUM)
    @ResponsePayload
    public GetExtendedMetadataResponse getExtendedMetadataAlbum(@RequestPayload GetExtendedMetadata request) {
        logger.info("GetExtendedMetadataAlbum");
        _verifyObjectIdPrefix(ALBUM, request);
        int albumId = parseIdFromRequest(ALBUM, request);

        GetExtendedMetadataResponse response = new GetExtendedMetadataResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        Album album = dataSource.getAlbum(albumId);

        ExtendedMetadata extendedMetadata = new ExtendedMetadata();
        extendedMetadata.setMediaCollection(MusicLibraryDataUtil.buildAlbumMediaCollection(album, source, getBaseRequestUrl()));

        List<RelatedText> relatedTexts = extendedMetadata.getRelatedText();
        dataSource.getAlbumInfo(albumId).stream().forEach(ai -> relatedTexts.add(buildRelatedText(source)
                .id(ALBUM + albumId)
                .type(ai.getType())
                .build()));

        response.setGetExtendedMetadataResult(extendedMetadata);
        return response;
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getExtendedMetadata")
    @BindObjectId(prefix = ARTIST)
    @ResponsePayload
    public GetExtendedMetadataResponse getExtendedMetadataArtist(@RequestPayload GetExtendedMetadata request) {
        logger.info("GetExtendedMetadataArtist");
        _verifyObjectIdPrefix(ARTIST, request);
        int artistId = parseIdFromRequest(ARTIST, request);

        GetExtendedMetadataResponse response = new GetExtendedMetadataResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        Artist artist = dataSource.getArtist(artistId);

        ExtendedMetadata extendedMetadata = new ExtendedMetadata();
        extendedMetadata.setMediaCollection(MusicLibraryDataUtil.buildArtistMediaCollection(artist, source, getBaseRequestUrl()));

        List<RelatedText> relatedTexts = extendedMetadata.getRelatedText();
        dataSource.getArtistInfo(artistId).stream().forEach(ai -> relatedTexts.add(buildRelatedText(source)
                .id(ARTIST + artistId)
                .type(ai.getType())
                .build()));

        response.setGetExtendedMetadataResult(extendedMetadata);
        return response;
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getExtendedMetadata")
    @BindObjectId(prefix = TRACK)
    @ResponsePayload
    public GetExtendedMetadataResponse getExtendedMetadataTrack(@RequestPayload GetExtendedMetadata request) {
        logger.info("GetExtendedMetadataTrack");
        _verifyObjectIdPrefix(TRACK, request);
        int trackId = parseIdFromRequest(TRACK, request);

        GetExtendedMetadataResponse response = new GetExtendedMetadataResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        Track track = dataSource.getTrack(trackId);

        ExtendedMetadata extendedMetadata = new ExtendedMetadata();
        extendedMetadata.setMediaMetadata(MusicLibraryDataUtil.buildTrackMediaMetadata(track, source, getBaseRequestUrl()));

        response.setGetExtendedMetadataResult(extendedMetadata);
        return response;
    }
}
