/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.model.DeviceLinkCredentials;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/deviceLink")
public class DeviceLinkPageController {

    private static final String AUTH_KEY_FORMAT = "user:%s@acmeMusic_%s";
    private static final String ERROR_VIEW = "error";
    private static final String ENTER_CODE_VIEW = "enterCode";
    private static final String SAVED_CODE_VIEW = "saved";
    @RequestMapping("/home")
    public String enterCode(@RequestParam(value="linkCode", required=false) String linkCode,
                            Model model) {
        if (null != linkCode && !"".equals(linkCode)) {
            model.addAttribute("linkCode", linkCode);
            DeviceLinkCredentials credentials = DeviceLinkCredentials.lookupDeviceLinkCredentialsByLinkCode(linkCode);
            if (null == credentials) {
                model.addAttribute("errorDesc", "invalid device link code");
                return ERROR_VIEW;
            } else {
                String linkDeviceId = credentials.getLinkDeviceId();
                model.addAttribute("deviceId", linkDeviceId);
            }
        }

        return ENTER_CODE_VIEW;
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String login(@RequestParam(value="deviceId") String linkDeviceId,
                        @RequestParam(value="linkCode") String linkCode,
                        @RequestParam(value="username") String username,
                        @RequestParam(value="password") String password,
                        Model model) {
        if (!validateDeviceCredentials(linkDeviceId, linkCode)) {
            model.addAttribute("errorDesc", "device credentials validation failed");
            return ERROR_VIEW;
        } else if (!validateUserCredentials(username, password)) {
            model.addAttribute("errorDesc", "user credentials validation failed");
            return ERROR_VIEW;
        } else {
            model.addAttribute("username", username);
            DeviceLinkCredentials credentials = DeviceLinkCredentials.lookupDeviceLinkCredentialsByLinkCode(linkCode);
            String privateKey = String.format(AUTH_KEY_FORMAT, username, RandomStringUtils.randomAlphanumeric(5));
            credentials.setAuthToken(DeviceLinkCredentials.generateAuthToken());
            credentials.setAuthKey(privateKey);
            DeviceLinkCredentials.storeDeviceLinkCredentialsByLinkCode(linkCode, credentials);
            return SAVED_CODE_VIEW;
        }
    }

    private boolean validateDeviceCredentials(String linkDeviceId, String linkCode) {
        DeviceLinkCredentials credentials = DeviceLinkCredentials.lookupDeviceLinkCredentialsByLinkCode(linkCode);
        return credentials != null &&
                credentials.getLinkDeviceId().equals(linkDeviceId);
    }

    // This will be replaced with an actual check against a user database
    private boolean validateUserCredentials(String username, String password) {
        return "acmeUser".equals(username) &&
                "password".equals(password);
    }
}
