/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.dao;

import com.sonos.contentapi.model.db.*;

import java.util.List;

public interface ContentApiDaoInterface {
    /**
     * This method gets a list of Albums for browsing.
     * @param index The index in the albums list where the result begins.
     * @param count The maximum number of albums to return.
     * @return A list of albums matching the requested index and count as best as possible.
     */
    List<Album> getAlbums(int index, int count);
    /*
     * This method gets a list of Albums matching a search query.
     * @param searchId Todo: Not sure what searchId is used for
     * @param query The query searched for.
     * @param index The index in the search response where the result begins.
     * @param count The maximum number of albums to return.
     * @return
     */
    List<Album> getSearchAlbums(String searchId, String query, int index, int count);

    /**
     * This method gets a list of Artists for browsing.
     * @param index The index in the artists list where the result begins.
     * @param count The maximum number of artists to return.
     * @return A list of artists matching the requested index and count as best as possible.
     */
    List<Artist> getArtists(int index, int count);
    /*
     * This method gets a list of Artists matching a search query.
     * @param searchId Todo: Not sure what searchId is used for
     * @param query The query searched for.
     * @param index The index in the search response where the result begins.
     * @param count The maximum number of artists to return.
     * @return
     */
    List<Artist> getSearchArtists(String searchId, String query, int index, int count);

    /**
     * This method gets a list of Tracks for browsing.
     * @param index The index in the tracks list where the result begins.
     * @param count The maximum number of tracks to return.
     * @return A list of tracks matching the requested index and count as best as possible.
     */
    List<Track> getTracks(int index, int count);
    /*
     * This method gets a list of Tracks matching a search query.
     * @param searchId Todo: Not sure what searchId is for
     * @param query The query searched for.
     * @param index The index in the search response where the result begins.
     * @param count The maximum number of tracks to return.
     * @return
     */
    List<Track> getSearchTracks(String searchId, String query, int index, int count);

    /**
     * This method gets a list of Tracks contained in an album.
     * @param albumId The albumId of the requested album.
     * @param index The index in the album's tracks list where the result begins.
     * @param count The maximum number of tracks to return.
     * @return A list of the album's tracks matching the requested index and count as best as possible.
     */
    List<Track> getAlbumTracks(int albumId, int index, int count);

    /**
     * This method gets a list of an artist's Albums.
     * @param artistId The artistId of the requested artist.
     * @param index The index in the artist's album list where the result begins.
     * @param count The maximum number of album to return.
     * @return A list of the artist's albums matching the requested index and count as best as possible.
     */
    List<Album> getArtistAlbums(int artistId, int index, int count);

    /**
     * This method gets a specific Album.
     * @param albumId The albumId of the requested album.
     * @return The album requested, if it exists. Null otherwise.
     */
    Album getAlbum(int albumId);

    /**
     * This method gets a specific Artist.
     * @param artistId The artistId of the requested artist.
     * @return The artist requested, if it exists. Null otherwise.
     */
    Artist getArtist(int artistId);

    /**
     * This method gets a specific Track.
     * @param trackId the trackId of the requested track.
     * @return the track requested, if it exists. Null otherwise.
     */
    Track getTrack(int trackId);

    /**
     * This method gets TrackInfo for a specific track.
     * @param trackId The trackId of the requested track.
     * @return The trackInfo for the requested track, if it exists. Null otherwise.
     */
    TrackInfo getTrackInfo(int trackId);

    /*
     * This method gets AlbumInfo for a specific album.
     * @param albumId The albumId of the requested album.
     * @param type The type of info desired ("ALBUM_NOTES" or a custom type)
     * @return The albumInfo for the requested album.
     */
    AlbumInfo getAlbumInfo(int albumId, String type);
    /*
     * This method gets all AlbumInfo for a specific album.
     * @param albumId The albumId of the requested album.
     * @return All known extra info about the album.
     */
    List<AlbumInfo> getAlbumInfo(int albumId);

    /*
     * This method gets ArtistInfo for a specific artist.
     * @param artistId The artistId of the requested artist.
     * @param type The type of info desired ("ARTIST_BIO" or a custom type)
     * @return The artistInfo for the requested artist.
     */
    ArtistInfo getArtistInfo(int artistId, String type);
    /*
     * This method gets all ArtistInfo for a specific artist.
     * @param artistId The artistId of the requested artist.
     * @return All known extra info about the artist.
     */
    List<ArtistInfo> getArtistInfo(int artistId);

    /**
     * This method gets the total count for an object type.
     * @param type The type of object which should be counted.
     * @return The total count of the requested object type.
     */
    int getAllCount(Class<?> type);
    /*
     * This method gets the count for an object type based on a restriction (e.g. all tracks for a particular album ID).
     * @param type The type of object which should be counted.
     * @param restriction How to constrain the results, e.g. "album.id"
     * @param id The ID of the related object by which to restrict
     * @return The total count of the requested object type.
     */
    int getCount(Class<?> type, String restriction, int id);

    /*
     * This method validates user credentials.
     * Todo: Not sure this is needed since credentials aren't currently stored in a datastore
     * @param username The user's username.
     * @param password The user's password.
     * @return A boolean indicating whether the user credentials are valid or not.
     */
    boolean isUserValid(String username, String password);
}
