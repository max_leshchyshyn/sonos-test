/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.AlbumInfo;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.ArtistInfo;
import com.sonos.contentapi.model.db.Artwork;
import com.sonos.contentapi.model.db.Track;
import com.sonos.contentapi.model.db.TrackInfo;
import com.sonos.contentapi.util.MusicLibraryDataUtil;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.sonos.contentapi.model.enums.RelatedTextType.ALBUM_NOTES;
import static com.sonos.contentapi.model.enums.RelatedTextType.ARTIST_BIO;

public class ContentApiDao implements ContentApiDaoInterface {
    private static final Logger logger = Logger.getLogger(ContentApiDao.class);
    private static List<Album> albums = new ArrayList<>();
    private static List<AlbumInfo> albumInfos = new ArrayList<>();
    private static List<Artist> artists = new ArrayList<>();
    private static List<ArtistInfo> artistInfos = new ArrayList<>();
    private static List<Track> tracks = new ArrayList<>();
    private static List<TrackInfo> trackInfos = new ArrayList<>();
    private static List<MusicEntry> musicLibrary = new ArrayList<>();
    private static final int musicCatalogSize = 100;

    static {
        // Populate musicLibrary with tracks listed in track.json
        ClassPathResource resource = new ClassPathResource("static/tracks.json");
        if (resource.exists()) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                InputStream tracksJson = resource.getInputStream();
                List<MusicEntry> trackList = mapper.readValue(tracksJson, mapper.getTypeFactory()
                        .constructCollectionType(List.class, MusicEntry.class));
                musicLibrary.addAll(trackList);
            } catch (IOException exception) {
                logger.error("Failed to read tracks.json");
            }
        }

        // Initialize a mocked catalog of music
        for (int i = 0; i < musicCatalogSize; i++) {
            final int musicLibraryIndex = (int) (100 * Math.random() % musicLibrary.size());
            final MusicEntry entry = musicLibrary.get(musicLibraryIndex);

            Album album;
            boolean isNewAlbum = false;
            List<Album> existingAlbums = albums.stream().filter(album1 -> album1.getTitle().equals(
                    entry.getAlbum())).collect(Collectors.toList());

            // Ensure there is at least 1 album with multiple tracks
            if (i >= (musicCatalogSize - 1) - musicLibrary.size() && existingAlbums.size() > 0) {
                album = existingAlbums.get(0);
            } else {
                isNewAlbum = true;
                album = new Album();
                album.setId(albums.size());
                album.setTitle(entry.getAlbum());
                Artwork art = new Artwork();
                art.setArtUrl(entry.getArtwork_url());
                album.setArt(art);
                albums.add(album);
            }

            AlbumInfo albumInfo = new AlbumInfo();
            albumInfo.setId(i);
            albumInfo.setAlbumId(album.getId());
            albumInfo.setDescription("This is album # " + i);
            albumInfo.setType(ALBUM_NOTES);
            albumInfos.add(albumInfo);

            Artist artist = new Artist();
            artist.setId(i);
            artist.setName(entry.getArtist());
            artist.setArt(album.getArt());
            artists.add(artist);

            // don't allow albums with null artists
            if (isNewAlbum) {
                album.setArtist(artist);
            }

            ArtistInfo artistInfo = new ArtistInfo();
            artistInfo.setId(i);
            artistInfo.setArtistId(artist.getId());
            artistInfo.setDescription("This is artist # " + i);
            artistInfo.setType(ARTIST_BIO);
            artistInfos.add(artistInfo);

            Track track = new Track();
            track.setId(i);
            track.setTitle(entry.getTitle());
            track.setAlbum(album);
            track.setArtist(artist);
            track.setMimeType("audio/mp3");
            int duration = MusicLibraryDataUtil.parseSecondsFromDurationString(entry.getDuration());
            if (duration > -1) {
                track.setDuration(duration);
            }
            tracks.add(track);

            TrackInfo trackInfo = new TrackInfo();
            trackInfo.setId(i);
            trackInfo.setTrack(track);
            trackInfo.setTrackUri(entry.getMedia_url());
            trackInfos.add(trackInfo);
        }
    }

    @Override
    public List<Album> getAlbums(int index, int count) {
        return safeSublist(albums, index, count);
    }

    @Override
    public List<Album> getSearchAlbums(String searchId, String query, int index, int count) {
        Predicate<Album> predicate = album -> Integer.toString(album.getId()).equals(query) ||
                album.getTitle().toLowerCase().contains(query.toLowerCase());
        return safeSublist(albums, index, count, predicate);
    }

    @Override
    public List<Artist> getArtists(int index, int count) {
        return safeSublist(artists, index, count);
    }

    @Override
    public List<Artist> getSearchArtists(String searchId, String query, int index, int count) {
        Predicate<Artist> predicate = artist -> Integer.toString(artist.getId()).equals(query) ||
                artist.getName().toLowerCase().contains(query.toLowerCase());
        return safeSublist(artists, index, count, predicate);
    }

    @Override
    public List<Track> getTracks(int index, int count) {
        return safeSublist(tracks, index, count);
    }

    @Override
    public List<Track> getSearchTracks(String searchId, String query, int index, int count) {
        Predicate<Track> predicate = track -> Integer.toString(track.getId()).equals(query) ||
                track.getTitle().toLowerCase().contains(query.toLowerCase());
        return safeSublist(tracks, index, count, predicate);
    }

    @Override
    public List<Track> getAlbumTracks(int albumId, int index, int count) {
        Predicate<Track> predicate = track -> track.getAlbum() != null &&  track.getAlbum().getId() == albumId;
        return safeSublist(tracks, index, count, predicate);
    }

    @Override
    public List<Album> getArtistAlbums(int artistId, int index, int count) {
        Predicate<Album> predicate = album -> album.getId() == artistId;
        return safeSublist(albums, index, count, predicate);
    }

    @Override
    public Album getAlbum(int albumId) {
        return albums.stream().filter(a -> a.getId() == albumId).findFirst().orElse(null);
    }

    @Override
    public Artist getArtist(int artistId) {
        return artists.stream().filter(a -> a.getId() == artistId).findFirst().orElse(null);
    }

    @Override
    public Track getTrack(int trackId) {
        return tracks.stream().filter(t -> t.getId() == trackId).findFirst().orElse(null);
    }

    @Override
    public TrackInfo getTrackInfo(int trackId) {
        return trackInfos.stream().filter(ti -> ti.getId() == trackId).findFirst().orElse(null);
    }

    @Override
    public AlbumInfo getAlbumInfo(int albumId, String type) {
        return albumInfos.stream().filter(ai -> ai.getAlbumId() == albumId).filter(ai -> ai.getType().equals(type)).findFirst().orElse(null);
    }

    @Override
    public List<AlbumInfo> getAlbumInfo(int albumId) {
        Predicate<AlbumInfo> predicate = albumInfo -> albumInfo.getAlbumId() == albumId;
        return albumInfos.stream().filter(predicate).collect(Collectors.toList());
    }

    @Override
    public ArtistInfo getArtistInfo(int artistId, String type) {
        return artistInfos.stream().filter(ai -> ai.getArtistId() == artistId).filter(ai -> ai.getType().equals(type)).findFirst().orElse(null);
    }

    @Override
    public List<ArtistInfo> getArtistInfo(int artistId) {
        Predicate<ArtistInfo> predicate = artistInfo -> artistInfo.getArtistId() == artistId;
        return artistInfos.stream().filter(predicate).collect(Collectors.toList());
    }

    private List<?> getCollectionForType(Class type) {
        if (type == Track.class) {
            return tracks;
        } else if (type == TrackInfo.class) {
            return trackInfos;
        } else if (type == Album.class) {
            return albums;
        } else if (type == AlbumInfo.class) {
            return albumInfos;
        } else if (type == Artist.class) {
            return artists;
        } else if (type == ArtistInfo.class) {
            return artistInfos;
        }
        return null;
    }

    @Override
    public int getAllCount(Class<?> type) {
        List<?> collection = getCollectionForType(type);
        if (collection == null) {
            return 0;
        }
        return collection.size();
    }

    @Override
    public int getCount(Class<?> type, String restriction, int id) {
        // right now we only support restrictions of the form "<relatedObject>.id". for example,
        // if type == Track.class and restriction.equals("album.id") we return
        // [track <- tracks | track.getAlbum().getId() == id]

        // make sure the restriction string looks right
        String restrictionElements[] = restriction.split("\\.");
        if (restrictionElements.length != 2 || !(restrictionElements[1].equals("id"))) {
            return 0;
        }

        // make sure we're trying to get a count of an object type we know about
        List<?> collection = getCollectionForType(type);
        if (collection == null) {
            return 0;
        }

        // make sure the type we're getting a count of actually has a getter for the specified constraint object type
        String constraintGetter = "get" +
                String.valueOf(restrictionElements[0].charAt(0)).toUpperCase() +
                restrictionElements[0].substring(1);
        Method getterMethod;
        try {
            getterMethod = type.getDeclaredMethod(constraintGetter);
        } catch (NoSuchMethodException | SecurityException ignored) {
            return 0;
        }

        long resultCount = collection.stream().filter(item -> {
            Object result;
            int constraintId;
            try {
                // if we've gotten this far we should be guaranteed there's a getId() method, but model.db.* don't have
                // a common subclass so we still have to use reflection
                // REVIEW: should model.db.* have a common subclass?
                Object constraintObject = getterMethod.invoke(item);
                Method idMethod;
                try {
                    idMethod = constraintObject.getClass().getDeclaredMethod("getId");
                } catch (NoSuchMethodException | SecurityException ignored) {
                    return false;
                }
                result = idMethod.invoke(constraintObject);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ignored) {
                return false;
            }

            if (!(result instanceof Integer)) {
                return false;
            }

            // now we can finally check whether this item passes our constraint
            constraintId = (Integer)result;
            return constraintId == id;
        }).count();

        // now we have the number of items of the specified type that pass the specified restriction.
        return (int)resultCount;
    }

    @Override
    public boolean isUserValid(String username, String password) {
        return true;
    }

    private <T> List<T> safeSublist(List<T> list, int index, int count) {
        return safeSublist(list, index, count, null);
    }

    private <T> List<T> safeSublist(List<T> list, int index, int count, Predicate<T> filter) {
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        }

        if (filter != null) {
            list = list.stream().filter(filter).collect(Collectors.toList());
        }

        if (index >= list.size()) {
            return new ArrayList<>();
        }
        int toIndex = index + count;
        if (toIndex > list.size()) {
            toIndex = list.size();
        }

        return list.subList(index, toIndex);
    }

    private static class MusicEntry {
        String title;
        String artist;
        String album;
        String duration;
        String media_url;
        String artwork_url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getAlbum() {
            return album;
        }

        public void setAlbum(String album) {
            this.album = album;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getMedia_url() {
            return media_url;
        }

        public void setMedia_url(String media_url) {
            this.media_url = media_url;
        }

        public String getArtwork_url() {
            return artwork_url;
        }

        public void setArtwork_url(String artwork_url) {
            this.artwork_url = artwork_url;
        }
    }
}
