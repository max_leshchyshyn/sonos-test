/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.contentapi.annotations.BindObjectId;
import com.sonos.contentapi.dao.ContentApiDaoInterface;
import com.sonos.contentapi.dao.impl.ContentApiDao;
import com.sonos.contentapi.model.db.Track;
import com.sonos.contentapi.model.db.TrackInfo;
import com.sonos.contentapi.util.MusicLibraryDataUtil;
import com.sonos.services._1.GetLastUpdate;
import com.sonos.services._1.GetLastUpdateResponse;
import com.sonos.services._1.GetMediaMetadata;
import com.sonos.services._1.GetMediaMetadataResponse;
import com.sonos.services._1.GetMediaURI;
import com.sonos.services._1.GetMediaURIResponse;
import com.sonos.services._1.GetSessionId;
import com.sonos.services._1.GetSessionIdResponse;
import com.sonos.services._1.LastUpdate;
import com.sonos.services._1.MediaMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.TRACK;

@Endpoint
public class MusicServiceEndpoint extends ContentApiDefaultEndpoint {
    @Autowired
    MessageSource source;

    private static final Logger logger = LoggerFactory.getLogger(MusicServiceEndpoint.class);
    private static final String CATALOG_VERSION = "12345";
    private static final String FAVORITES_VERSION = "23456";

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getSessionId")
    @ResponsePayload
    public GetSessionIdResponse getSessionId(@RequestPayload final  GetSessionId request) {
        logger.info("GetSessionId");

        return new GetSessionIdResponse();
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMediaMetadata")
    @BindObjectId(prefix = TRACK)
    @ResponsePayload
    public GetMediaMetadataResponse getMediaMetadataTrack(@RequestPayload GetMediaMetadata request) {
        logger.info("GetMediaMetadata");

        _verifyObjectIdPrefix(TRACK, request);
        int trackId = parseIdFromRequest(TRACK, request);

        GetMediaMetadataResponse response = new GetMediaMetadataResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        Track track = dataSource.getTrack(trackId);
        MediaMetadata mediaMetadata = MusicLibraryDataUtil.buildTrackMediaMetadata(track, source, getBaseRequestUrl());

        response.setGetMediaMetadataResult(mediaMetadata);
        return response;
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getMediaURI")
    @BindObjectId(prefix = TRACK)
    @ResponsePayload
    public GetMediaURIResponse getMediaUriTrack(@RequestPayload GetMediaURI request) {
        logger.info("GetMediaURI");

        _verifyObjectIdPrefix(TRACK, request);
        int trackId = parseIdFromRequest(TRACK, request);

        GetMediaURIResponse response = new GetMediaURIResponse();
        ContentApiDaoInterface dataSource = new ContentApiDao();
        TrackInfo trackInfo = dataSource.getTrackInfo(trackId);
        response.setGetMediaURIResult(ContentApiDefaultEndpoint.getBaseRequestUrl() + trackInfo.getTrackUri());
        return response;
    }

    @SuppressWarnings("unused")
    @PayloadRoot(namespace = MusicServiceConfig.SONOS_SOAP_NAMESPACE, localPart = "getLastUpdate")
    @ResponsePayload
    public GetLastUpdateResponse getLastUpdate(@RequestPayload GetLastUpdate request) {
        logger.info("GetLastUpdate");
        GetLastUpdateResponse getLastUpdateResponse = new GetLastUpdateResponse();
        LastUpdate lastUpdate = new LastUpdate();
        getLastUpdateResponse.setGetLastUpdateResult(lastUpdate);

        lastUpdate.setCatalog(CATALOG_VERSION);
        lastUpdate.setFavorites(FAVORITES_VERSION);
        lastUpdate.setPollInterval(MusicServiceConfig.getPollInterval());

        return getLastUpdateResponse;
    }
}
