/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi;

import com.sonos.services._1.CustomFaultDetail;
import org.slf4j.Logger;
import org.springframework.ws.client.WebServiceFaultException;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.saaj.SaajSoapMessageException;
import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ContentApiDefaultEndpoint {

    private static final int SONOS_ERROR_CODE_NYI = 1000;

    private String getIdFromRequest(Object request) {
        Method getIdMethod;
        try {
            getIdMethod = request.getClass().getDeclaredMethod("getId");
        } catch (NoSuchMethodException e) {
            throw new SaajSoapMessageException(e.getMessage());
        }

        String requestId;
        try {
            requestId = (String) getIdMethod.invoke(request);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new SaajSoapMessageException(e.getMessage());
        }

        return requestId;
    }

    protected void _verifyObjectId(String id, Object request) {
        String requestId = getIdFromRequest(request);

        if (!id.equals(requestId)) {
            String err = String.format("Message bound to wrong method. Expected Object ID of %s got %s", id, requestId);
            throw new SaajSoapMessageException(err);
        }
    }

    protected void _verifyObjectIdPrefix(String prefix, Object request) {
        String requestId = getIdFromRequest(request);

        if (!requestId.startsWith(prefix)) {
            String err = String.format("Message bound to wrong method. Expected Object ID prefix %s and ID is %s", prefix, requestId);
            throw new SaajSoapMessageException(err);
        }
    }

    private String _removePrefixFromId(String prefix, Object request) {
        String requestId = getIdFromRequest(request);

        if (!requestId.startsWith(prefix)) {
            String err = String.format("Message bound to wrong method. Expected Object ID prefix %s and ID is %s", prefix, requestId);
            throw new SaajSoapMessageException(err);
        }
        return requestId.substring(prefix.length());
    }

    protected int parseIdFromRequest(String prefix, Object request) {
        return Integer.parseInt(_removePrefixFromId(prefix, request));
    }

    protected static void logException(Logger logger, Throwable ex) {
        logException(logger, ex, null);
    }

    protected static void logException(Logger logger, Throwable ex, String message) {
        assert ex != null;
        ByteArrayOutputStream trace = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(trace);
        ex.printStackTrace(out);
        message = (message != null ? message + ": " : "") + out.toString();
        logger.error(message);
    }

    static String getBaseRequestUrl() {
        String baseUrl = "http://localhost:8080";
        TransportContext transportContext = TransportContextHolder.getTransportContext();

        if (transportContext != null) {
            WebServiceConnection conn = transportContext.getConnection();
            try {
                URI connUri = conn.getUri();
                baseUrl = new URL(connUri.getScheme(), connUri.getHost(), connUri.getPort(), "").toString();
            } catch (URISyntaxException | MalformedURLException e) {
                // Since we are constructing the URL object directly from the properties of a URI object, it should be
                // impossible to throw an exception here.
                throw new WebServiceFaultException(e.getMessage());

            }
        }

        return baseUrl;
    }

    CustomFaultDetail _defaultResponse(@SuppressWarnings("UnusedParameters") SoapHeader requestHeader, MessageContext messageContext) {
        CustomFaultDetail detail = new CustomFaultDetail();
        detail.setExceptionInfo("Method currently unimplemented: " + messageContext.getRequest().toString());
        detail.setSonosError(SONOS_ERROR_CODE_NYI);

        return detail;
    }
}
