/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.util;

import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Track;
import com.sonos.services._1.DynamicData;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaCollection;
import com.sonos.services._1.MediaMetadata;
import com.sonos.services._1.TrackMetadata;
import org.springframework.context.MessageSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.*;
import static com.sonos.contentapi.util.media.builders.DynamicDataBuilder.buildDynamicData;
import static com.sonos.contentapi.util.media.builders.MediaCollectionBuilder.buildMediaCollection;
import static com.sonos.contentapi.util.media.builders.MediaMetadataBuilder.buildMediaMetadata;
import static com.sonos.contentapi.util.media.builders.TrackMetadataBuilder.buildTrackMetadata;

public class MusicLibraryDataUtil {
    private static final String TRACK_PROPERTY_LIKED = "LIKED";
    private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

    public static int parseSecondsFromDurationString(String duration) {
        Date d;
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        try {
            d = formatter.parse(duration);
            start.setTime(formatter.parse("00:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
        end.setTime(d);
        return (int)((end.getTimeInMillis() - start.getTimeInMillis()) / 1000);
    }

    public static MediaMetadata buildTrackMediaMetadata(Track track, MessageSource source, String baseUrl) {
        TrackMetadata trackMetadata = buildTrackMetadata(source)
                .canPlay(true)
                .canAddToFavorites(true)
                .album(track.getAlbum())
                .artist(track.getArtist())
                .duration(track.getDuration())
                .art(track.getAlbum().getArt(), baseUrl)
                .build();
        DynamicData dynamicData = buildDynamicData(source)
                .property(TRACK_PROPERTY_LIKED, String.valueOf(track.getRatingValue()))
                .build();
        return buildMediaMetadata(source)
                .itemType(ItemType.TRACK)
                .id(TRACK + track.getId())
                .title(track.getTitle())
                .mimeType(track.getMimeType())
                .favorite(track.isFavorite())
                .trackMetadata(trackMetadata)
                .dynamic(dynamicData)
                .build();
    }

    public static MediaCollection buildAlbumMediaCollection(Album album, MessageSource source, String baseUrl) {
        return buildMediaCollection(source)
                .id(ALBUM + album.getId())
                .title(album.getTitle())
                .itemType(ItemType.ALBUM)
                .artist(album.getArtist())
                .art(album.getArt(), baseUrl)
                .canPlay(true)
                .canEnumerate(true)
                .canAddToFavorites(true)
                .isFavorite(album.isFavorite())
                .build();
    }

    public static MediaCollection buildArtistMediaCollection(Artist artist, MessageSource source, String baseUrl) {
        return buildMediaCollection(source)
                .itemType(ItemType.ARTIST)
                .id(ARTIST + artist.getId())
                .title(artist.getName())
                .art(artist.getArt(), baseUrl)
                .canPlay(false)
                .canEnumerate(true)
                .build();
    }
}
