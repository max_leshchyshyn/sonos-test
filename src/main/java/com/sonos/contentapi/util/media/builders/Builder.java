/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.util.media.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;

import java.nio.charset.StandardCharsets;

/**
 * This generic class helps in applying the Builder pattern to different types of media as well as localize any string
 * data passed in. Any user of a builder will need to be able to return an instance of the media type being build
 * through the <code>build()</code> method. Similarly, any specialization of this class will need to be able to localize
 * strings using the protected <code>localize(String id)</code> method. Not all strings are required to be localized
 * and not all instances of the builder a required to localize. By constructing the specialized classes with a
 * MessageSource, users can indicate their desire to localize. Otherwise, the buidler sets the field to the original
 * string passed in with no transformations performed.
 *
 * @param <T> This parameterized type represents the type of media being built using this Builder pattern.
 */
public class Builder<T>  {
    private static final Logger logger = LoggerFactory.getLogger(Builder.class);
    protected static final String PREFIX = "displayName.";

    private MessageSource source; // Message source for localization
    protected T built; // Instance of media type being built

    protected Builder(T built, MessageSource source) {
        this.built = built;
        this.source = source;
    }

    public T build() {
        return built;
    }

    protected String localize(String id) {
        String title = id;
        if(source != null && id != null) {
            try {
                String localizedTitle = source.getMessage(PREFIX + id.replace(":", "-"), null, LocaleContextHolder.getLocale());
                title = localizedTitle == null ? title : new String(localizedTitle.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            } catch(NoSuchMessageException e) {
                logger.warn("Unable to find localized string for " + id + ". Returning original string");
            }
        }

        return title;
    }
}
