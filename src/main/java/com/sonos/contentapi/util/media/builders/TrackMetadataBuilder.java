/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.util.media.builders;

import com.sonos.contentapi.model.db.Album;
import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Artwork;
import com.sonos.services._1.AlbumArtUrl;
import com.sonos.services._1.TrackMetadata;
import org.springframework.context.MessageSource;

import static com.sonos.contentapi.model.enums.ObjectIdPrefix.ALBUM;
import static com.sonos.contentapi.model.enums.ObjectIdPrefix.ARTIST;

public class TrackMetadataBuilder extends Builder<TrackMetadata> {
    public static TrackMetadataBuilder buildTrackMetadata(MessageSource context) {
        return new TrackMetadataBuilder(context);
    }

    public static TrackMetadataBuilder buildTrackMetadata() {
        return new TrackMetadataBuilder(null);
    }

    protected TrackMetadataBuilder(MessageSource source) {
        super(new TrackMetadata(), source);
    }

    public TrackMetadataBuilder canPlay(boolean canPlay) {
        built.setCanPlay(canPlay);
        return this;
    }

    public TrackMetadataBuilder canAddToFavorites(boolean canAddToFavorites) {
        built.setCanAddToFavorites(canAddToFavorites);
        return this;
    }

    public TrackMetadataBuilder album(Album album) {
        built.setAlbum(album.getTitle());
        built.setAlbumId(ALBUM + album.getId());
        return this;
    }

    public TrackMetadataBuilder artist(Artist artist) {
        built.setArtist(artist.getName());
        built.setArtistId(ARTIST + artist.getId());
        return this;
    }

    public TrackMetadataBuilder duration(int duration) {
        built.setDuration(duration);
        return this;
    }

    public TrackMetadataBuilder art(Artwork art, String baseUrl) {
        AlbumArtUrl albumArtUrl = new AlbumArtUrl();
        albumArtUrl.setValue(baseUrl + art.getArtUrl());
        built.setAlbumArtURI(albumArtUrl);
        return this;
    }
}
