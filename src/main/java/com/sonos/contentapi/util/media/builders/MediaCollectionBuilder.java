/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.util.media.builders;

import com.sonos.contentapi.model.db.Artist;
import com.sonos.contentapi.model.db.Artwork;
import com.sonos.contentapi.model.enums.ObjectIdPrefix;
import com.sonos.services._1.AlbumArtUrl;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaCollection;
import org.springframework.context.MessageSource;

/**
 * This class builds Item objects for responses to getMetadata requests and other endpoint methods. It also localizes
 * both the title and summary fields of the objects.
 *
 * @see Builder
 */
public class MediaCollectionBuilder extends Builder<MediaCollection> {

    public static MediaCollectionBuilder buildMediaCollection(MessageSource context) {
        return new MediaCollectionBuilder(context);
    }

    public static MediaCollectionBuilder buildMediaCollection() {
        return new MediaCollectionBuilder(null);
    }

    protected MediaCollectionBuilder(MessageSource source) {
        super(new MediaCollection(), source);
    }

    public MediaCollectionBuilder id(String id) {
        built.setId(id);
        return this;
    }

    public MediaCollectionBuilder title(String titleId) {
        built.setTitle(localize(titleId));
        return this;
    }

    public MediaCollectionBuilder itemType(ItemType type) {
        built.setItemType(type);
        return this;
    }

    public MediaCollectionBuilder art(Artwork art, String baseUrl) {
        AlbumArtUrl albumArtUrl = new AlbumArtUrl();
        albumArtUrl.setValue(baseUrl + art.getArtUrl());
        built.setAlbumArtURI(albumArtUrl);
        return this;
    }

    public MediaCollectionBuilder artist(Artist artist) {
        built.setArtist(artist.getName());
        built.setArtistId(ObjectIdPrefix.ARTIST + artist.getId());
        return this;
    }

    public MediaCollectionBuilder canPlay(boolean canPlay) {
        built.setCanPlay(canPlay);
        return this;
    }

    public MediaCollectionBuilder canEnumerate(boolean canEnumerate) {
        built.setCanEnumerate(canEnumerate);
        return this;
    }

    public MediaCollectionBuilder canAddToFavorites(boolean canAddToFavorites) {
        built.setCanAddToFavorites(canAddToFavorites);
        return this;
    }

    public MediaCollectionBuilder isFavorite(boolean isFavorite) {
        built.setIsFavorite(isFavorite);
        return this;
    }

    public MediaCollectionBuilder containsFavorite(boolean favorite) {
        built.setContainsFavorite(favorite);
        return this;
    }

    public MediaCollectionBuilder displayType(String type) {
        built.setDisplayType(type);
        return this;
    }

    public MediaCollectionBuilder summary(String summary) {
        built.setSummary(localize(summary));
        return this;
    }
}
