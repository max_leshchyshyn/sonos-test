/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.util.media.builders;

import com.sonos.services._1.DynamicData;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaMetadata;
import com.sonos.services._1.TrackMetadata;
import org.springframework.context.MessageSource;

public class MediaMetadataBuilder extends Builder<MediaMetadata> {
    public static MediaMetadataBuilder buildMediaMetadata(MessageSource context) {
       return new MediaMetadataBuilder(context);
    }

    public static MediaMetadataBuilder buildMediaMetadata() {
        return new MediaMetadataBuilder(null);
    }

    protected MediaMetadataBuilder(MessageSource source) {
        super(new MediaMetadata(), source);
    }

    public MediaMetadataBuilder id(String id) {
        built.setId(id);
        return this;
    }

    public MediaMetadataBuilder itemType(ItemType itemType) {
        built.setItemType(itemType);
        return this;
    }

    public MediaMetadataBuilder title(String title) {
        built.setTitle(title);
        return this;
    }

    public MediaMetadataBuilder mimeType(String mimeType) {
        built.setMimeType(mimeType);
        return this;
    }

    public MediaMetadataBuilder favorite(boolean favorite) {
        built.setIsFavorite(favorite);
        return this;
    }

    public MediaMetadataBuilder trackMetadata(TrackMetadata trackMetadata) {
        built.setTrackMetadata(trackMetadata);
        return this;
    }

    public MediaMetadataBuilder dynamic(DynamicData data) {
        built.setDynamic(data);
        return this;
    }
}
