/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */
package com.sonos.contentapi.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation allows for finer grained routing when used in conjunction with the @PayloadRoot annotation.
 * By either matching an object id to the entire value() field or by matching the start of the object id the prefix(),
 * you can route a message to a specific endpoint method handler. This provides greater control than just the @PayloadRoot
 * which can only match the namespace and localPart.
 *
 * In the event that both the value() and prefix() fields are set, the value() field overrides prefix(). Though, in
 * practice, both should not be set.
 *
 * @see org.springframework.ws.server.endpoint.annotation.PayloadRoot
 * @see org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping
 * @see com.sonos.contentapi.MusicServiceAnnotationMethodEndpointMapping
 */
@Target(value=METHOD)
@Retention(value=RUNTIME)
@Documented
public @interface BindObjectId {
    String value() default "";
    String prefix() default "";
}
