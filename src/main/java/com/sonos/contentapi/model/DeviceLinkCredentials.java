/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.model;

import com.sonos.contentapi.security.ContentApiLoginToken;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * DeviceLinkCredentials is a simple container for key data used in the DeviceLink authentication mode.
 * It is a 4-tuple of device ID (an optional value, not displayed to the user, to help secure against phishing),
 * Household ID (uniquely identifying the Sonos user), authentication token (a token authorizing all players in the
 * Sonos household to browse and play content from the music service), and authentication key (optional complementary
 * authentication data, if necessary to precisely identify an account to the music service).
 */
public class DeviceLinkCredentials {
    private String linkDeviceId;
    private String householdId;
    private String authToken;
    private String authKey;

    private static Map<String, DeviceLinkCredentials> deviceLinkCodeMap = new HashMap<>();
    private static Map<String, DeviceLinkCredentials> deviceLinkHHIDMap = new HashMap<>();

    private static final long AUTH_DURATION = 300000;

    /**
     * Represents the status of an access token.
     * <ul>
     *     <li><strong>INVALID</strong>: the provided token does not have a recognized household ID or refresh token</li>
     *     <li><strong>VALID</strong>: the provided token is valid</li>
     *     <li><strong>EXPIRED</strong>: the provided token's household ID and refresh token are valid, but the access token has expired</li>
     * </ul>
     */
    public enum AuthTokenStatus {
        INVALID,
        VALID,
        EXPIRED
    }

    /**
     * Default constructor
     */
    public DeviceLinkCredentials() {
        /* empty */
    }

    /**
     * Convenience constructor for DeviceLinkCredentials
     * @param linkDeviceId optional: a token hidden from the user to guard against phishing
     * @param householdId the ID of the Sonos Household requesting access to the music service account
     * @param authToken the authorization token for the household to enable browsing and playback
     * @param authKey optional: additional data needed to authorize use of the token
     */
    public DeviceLinkCredentials(String linkDeviceId, String householdId, String authToken, String authKey) {
        this.linkDeviceId = linkDeviceId;
        this.householdId = householdId;
        this.authToken = authToken;
        this.authKey = authKey;
    }

    private DeviceLinkCredentials(DeviceLinkCredentials other) {
        this(other.getLinkDeviceId(), other.getHouseholdId(), other.getAuthToken(), other.getAuthKey());
    }

    /**
     * Utility method to generate an authentication token.
     * DISCLAIMER: For the purposes of ease of testing only, the auth token was chosen to be a timestamp (in future).
     * Please note that this is NOT recommended for production code.
     * @return a newly generated authentication token
     */
    public static String generateAuthToken() {
        long token = Instant.now().plusMillis(AUTH_DURATION).toEpochMilli();
        return String.valueOf(token);
    }

    /**
     * Saves device link credentials in the server mapping during the linking process.
     * @param linkCode the generated device link code
     * @param credentials the device link credentials to associate with the link code. Must contain a non-null device ID and household ID
     * @return true if the device link credentials were successfully stored
     */
    public static boolean storeDeviceLinkCredentialsByLinkCode(String linkCode, DeviceLinkCredentials credentials) {
        if (credentials.getLinkDeviceId() != null && credentials.getHouseholdId() != null) {
            deviceLinkCodeMap.put(linkCode, credentials);
            return true;
        }

        return false;
    }

    /**
     * Store the current device credentials after the account has been linked, so the access token can be checked later.
     * @param credentials a DeviceLinkCredentials object associating a household ID with an auth token and a refresh token.
     * @return true if the credentials were successfully stored
     */
    public static boolean storeDeviceLinkCredentialsByHouseholdId(DeviceLinkCredentials credentials) {
        if (credentials.getLinkDeviceId() != null &&
                credentials.getHouseholdId() != null &&
                credentials.getAuthToken() != null &&
                credentials.getAuthKey() != null) {
            deviceLinkHHIDMap.put(credentials.getHouseholdId(), credentials);
            return true;
        }

        return false;
    }

    /**
     * Looks up the device link credentials for a link code.
     * @param linkCode the device link code to look up
     * @return the device link credentials associated with the link code, or null if not found
     */
    public static DeviceLinkCredentials lookupDeviceLinkCredentialsByLinkCode(String linkCode) {
        DeviceLinkCredentials credentials = deviceLinkCodeMap.get(linkCode);
        if (credentials != null) {
            return new DeviceLinkCredentials(credentials);
        } else {
            return null;
        }
    }

    /**
     * Looks up the device link credentials for an already-linked household
     * @param householdId the household ID of the linked household
     * @return the device link credentials stored for the linked household, or null if not found
     */
    public static DeviceLinkCredentials lookupDeviceLinkCredentialsByHouseholdId(String householdId) {
        DeviceLinkCredentials credentials = deviceLinkHHIDMap.get(householdId);
        if (credentials != null) {
            return new DeviceLinkCredentials(credentials);
        } else {
            return null;
        }
    }

    /**
     * Return the status of a login token provided in SOAP header credentials.
     * @param loginToken the login token provided by the client
     * @return an AuthTokenStatus constant: INVALID, EXPIRED, or VALID
     */
    public static AuthTokenStatus checkAuthToken(ContentApiLoginToken loginToken) {
        if (!deviceLinkHHIDMap.containsKey(loginToken.getHouseholdId())) {
            return AuthTokenStatus.INVALID;
        }

        DeviceLinkCredentials credentials = deviceLinkHHIDMap.get(loginToken.getHouseholdId());
        if (!credentials.getAuthKey().equals(loginToken.getKey())) {
            return AuthTokenStatus.INVALID;
        }

        Instant expiry = Instant.ofEpochMilli(Long.parseLong(loginToken.getToken()));
        if (expiry.isBefore(Instant.now())) {
            return AuthTokenStatus.EXPIRED;
        }

        return AuthTokenStatus.VALID;
    }

    /**
     * Removes a device link code mapping once the device link is set up.
     * @param linkCode the link code to remove
     */
    public static void removeLinkCodeMapping(String linkCode) {
        if (deviceLinkCodeMap.containsKey(linkCode)) {
            deviceLinkCodeMap.remove(linkCode);
        }
    }

    /**
     * Gets the current linked device ID.
     * @return the current linked device ID
     */
    public String getLinkDeviceId() {
        return linkDeviceId;
    }

    /**
     * Sets the linked device ID.
     * @param linkDeviceId the new linked device ID
     */
    public void setLinkDeviceId(String linkDeviceId) {
        this.linkDeviceId = linkDeviceId;
    }

    /**
     * Gets the current Sonos Household ID.
     * @return the current Sonos Household ID
     */
    public String getHouseholdId() {
        return householdId;
    }

    /**
     * Sets the Sonos Household ID.
     * @param householdId the new Sonos Household ID
     */
    public void setHouseholdId(String householdId) {
        this.householdId = householdId;
    }

    /**
     * Gets the current authentication token.
     * @return the current authentication token
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Sets a new authentication token.
     * @param authToken the new authentication token
     */
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     * Gets the current authentication key.
     * @return the current authentication key
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * Sets a new authentication key.
     * @param authKey the new authentication key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
}
