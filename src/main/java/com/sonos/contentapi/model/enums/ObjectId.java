/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.model.enums;

/**
 * This class defines Object IDs for routing ContentAPI requests to the proper handler method.
 */
public class ObjectId {
    public static final String ROOT = "root";
    public static final String SEARCH = "search";
    public static final String BROWSE_ALBUMS = "browse:albums";
    public static final String BROWSE_ALBUMS_V2 = "browse:albums_v2";
    public static final String BROWSE_ARTISTS = "browse:artists";
    public static final String BROWSE_RADIO = "browse:radio";
    public static final String BROWSE_TRACKS = "browse:tracks";
    public static final String BROWSE_TOTM = "browse:totm";
    public static final String BROWSE_LISTEN_NOW = "browse:listenNow";
    public static final String BROWSE_PLAYLISTS = "browse:playlists";
    public static final String BROWSE_FAVORITES = "browse:favorites";
    public static final String BROWSE_FAVORITE_TRACKS = "browse:favorite-tracks";
    public static final String BROWSE_FAVORITE_ALBUMS = "browse:favorite-albums";
    public static final String BROWSE_CONCIERGE = "browse:concierge";
    public static final String CONCIERGE_GENRE = "concierge:genre";
}
