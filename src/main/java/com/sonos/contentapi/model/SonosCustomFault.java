/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.model;

import com.sonos.contentapi.MusicServiceConfig;
import com.sonos.services._1.CustomFaultDetail;
import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.CUSTOM, customFaultCode = SonosCustomFault.SONOS_SERVER_FAULT_CODE)
public class SonosCustomFault extends Exception {
    private final CustomFaultDetail customFaultDetail;

    // These are static final Strings instead of an enum because annotation attribute bundles have to be constant
    public static final String SONOS_SERVER_FAULT_CODE = "{" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "}Server";
    public static final String SONOS_CLIENT_FAULT_CODE = "{" + MusicServiceConfig.SONOS_SOAP_NAMESPACE + "}Client";
    public static final String UNKNOWN_FAULT_SUBCODE = "UNKNOWN";
    public static final String NOT_LINKED_RETRY_FAULT_SUBCODE = "NOT_LINKED_RETRY";
    public static final String NOT_LINKED_FAILURE_FAULT_SUBCODE = "NOT_LINKED_FAILURE";
    public static final String UNIMPLEMENTED_FAULT_SUBCODE = "UNIMPLEMENTED";
    public static final String AUTH_TOKEN_REFRESH_REQUIRED_SUBCODE = "TokenRefreshRequired";
    public static final String AUTH_TOKEN_EXPIRED_SUBCODE = "AuthTokenExpired";

    protected static enum SonosErrorCode {
        UNKNOWN(-1),
        NOT_LINKED_RETRY(5),
        NOT_LINKED_FAILURE(6),
        UNAUTHORIZED(401),
        UNIMPLEMENTED(1000);

        private final int value;
        private SonosErrorCode(int v) {
            value = v;
        }

        public int getValue() {
            return value;
        }

        public static SonosErrorCode fromValue(int v) {
            for (SonosErrorCode c : SonosErrorCode.values()) {
                if (c.value == v) {
                    return c;
                }
            }
            throw new IllegalArgumentException("" + v);
        }
    }

    public SonosCustomFault(String message, int sonosErrorCode, String exceptionInfo) {
        this(message, sonosErrorCode, exceptionInfo, null);
    }

    public SonosCustomFault(String message, int sonosErrorCode, String exceptionInfo, Throwable cause) {
        super(message, cause);
        customFaultDetail = new CustomFaultDetail();
        customFaultDetail.setSonosError(sonosErrorCode);
        customFaultDetail.setExceptionInfo(exceptionInfo);
    }

    /**
     * Gets the custom fault detail for this fault, which includes a Sonos-specific error faultCode used in communications
     * between the player and controller
     * @return a CustomFaultDetail object
     */
    public CustomFaultDetail getCustomFaultDetail() {
        return customFaultDetail;
    }
}
