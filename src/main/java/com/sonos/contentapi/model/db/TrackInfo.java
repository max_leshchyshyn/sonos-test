/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.model.db;

import javax.persistence.*;

@Entity
@Table(name="trackInfo")
public class TrackInfo {
    @Id
    @Column(name="id")
    private int id;

    @Column(name="isDRM")
    private boolean isDRM;

    @Column(name="trackUri")
    private String trackUri;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="trackId")
    private Track track;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDRM() { return isDRM; }

    public void setIsDRM(boolean isDRM) { this.isDRM = isDRM; }

    public String getTrackUri() {
        return trackUri;
    }

    public void setTrackUri(String trackUri) { this.trackUri = trackUri; }

    public Track getTrack() { return track; }

    public void setTrack(Track track) { this.track = track; }
}
