/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.security;

import com.sonos.contentapi.model.SonosCustomFault;
import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

/**
 * Used to indicate that the client's access token has expired. If the server does not support automatic token refresh,
 * a client must reauthorize when the access token expires.
 */
@SoapFault(faultCode = FaultCode.CUSTOM,
        customFaultCode = SonosCustomFault.SONOS_CLIENT_FAULT_CODE + "." + SonosCustomFault.AUTH_TOKEN_EXPIRED_SUBCODE,
        faultStringOrReason = SonosCustomFault.AUTH_TOKEN_EXPIRED_SUBCODE)
public class AuthTokenExpiredException extends SonosCustomFault {
    public AuthTokenExpiredException() {
        super(AUTH_TOKEN_EXPIRED_SUBCODE, 0, AUTH_TOKEN_EXPIRED_SUBCODE);
    }
}
