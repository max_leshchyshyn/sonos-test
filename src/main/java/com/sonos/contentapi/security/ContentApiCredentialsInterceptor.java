/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Sonos, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

package com.sonos.contentapi.security;

import com.sonos.contentapi.model.DeviceLinkCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.dom.DOMSource;

/**
 * This Interceptor checks for Content API credentials in the SOAP message header.
 * If the server uses Device Link authentication, the getDeviceLinkCode/getDeviceAuthToken handshake is used to
 * acquire an access token and refresh token; for all other Content API messages, those tokens as well as the
 * Household ID from the getDeviceAuthToken call must be included in the credentials.
 */
@Component
public class ContentApiCredentialsInterceptor implements EndpointInterceptor {
    private static Logger logger = LoggerFactory.getLogger(ContentApiCredentialsInterceptor.class);

    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        SaajSoapMessage soapMessage = (SaajSoapMessage) messageContext.getRequest();
        DOMSource domSource = (DOMSource) soapMessage.getSoapHeader().getSource();
        Node headerNode = domSource.getNode();
        ContentApiCredentials credentials = getCredentialsFromHeader(headerNode);
        if (credentials == null) {
            logger.error("credentials not found in header");
            // use the "Expired" exception to tell Sonos the account needs to be (re-)authorized
            throw new AuthTokenExpiredException();
        } else {
            // examine the message: the Device Link handshake only requires some credential information
            String command = ((DOMSource) soapMessage.getSoapBody().getPayloadSource()).getNode().getLocalName();
            switch (command) {
                case "getDeviceLinkCode":
                case "getDeviceAuthToken":
                    // we do not need a loginToken here: this is how we get one in the first place!
                    break;
                default:
                    // for all other Content API calls, we do need a loginToken.
                    if (credentials.getLoginToken() == null ||
                            isNullOrEmpty(credentials.getLoginToken().getToken()) ||
                            isNullOrEmpty(credentials.getLoginToken().getKey()) ||
                            isNullOrEmpty(credentials.getLoginToken().getHouseholdId())) {
                        logger.warn("not performing device link handshake, but login token is missing or incomplete");
                        throw new AuthTokenExpiredException();
                    }

                    DeviceLinkCredentials.AuthTokenStatus status =
                            DeviceLinkCredentials.checkAuthToken(credentials.getLoginToken());

                    switch (status) {
                        case INVALID:
                            throw new AuthTokenExpiredException();
                        case VALID:
                            break;
                        case EXPIRED:
                            String newToken = DeviceLinkCredentials.generateAuthToken();
                            DeviceLinkCredentials storedCredentials =
                                    DeviceLinkCredentials.
                                            lookupDeviceLinkCredentialsByHouseholdId(credentials.
                                                    getLoginToken().getHouseholdId());
                            // if the household ID wasn't found, checkAuthToken would return INVALID, not EXPIRED,
                            // so storedCredentials can't be null here.
                            assert storedCredentials != null;
                            storedCredentials.setAuthToken(newToken);
                            DeviceLinkCredentials.storeDeviceLinkCredentialsByHouseholdId(storedCredentials);
                            // for simplicity, in this implementation refresh tokens do not expire.
                            // in a real-world implementation, they may be longer-lived than auth tokens, but would
                            // normally also be refreshed from time to time.
                            throw new AuthTokenRefreshRequiredException(newToken, storedCredentials.getAuthKey());
                        default:
                            break;
                    }
                    break;
            }
        }
        return true;
    }

    private boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    /**
     * Convenience method for pulling a &lt;credentials&gt; element out of a SOAP header
     * @param headerNode the Node of the header element
     * @return a ContentApiCredentials object containing the credentials provided in the header, or null
     */
    private ContentApiCredentials getCredentialsFromHeader(Node headerNode) {
        if (headerNode == null || !headerNode.hasChildNodes()) {
            return null;
        }

        ContentApiCredentials credentials = new ContentApiCredentials();
        Node credentialsNode = getChildByName(headerNode, "credentials");
        if (credentialsNode == null) {
            return null;
        }

        Node deviceIdNode = getChildByName(credentialsNode, "deviceId");
        if (deviceIdNode == null) {
            return null;
        }
        credentials.setDeviceId(deviceIdNode.getTextContent());

        Node deviceProviderNode = getChildByName(credentialsNode, "deviceProvider");
        if (deviceProviderNode == null) {
            return null;
        }
        credentials.setDeviceProvider(deviceProviderNode.getTextContent());

        Node loginTokenNode = getChildByName(credentialsNode, "loginToken");
        if (loginTokenNode == null) {
            credentials.setLoginToken(null);
            return credentials;
        }

        ContentApiLoginToken loginToken = new ContentApiLoginToken();
        credentials.setLoginToken(loginToken);
        Node tokenNode = getChildByName(loginTokenNode, "token");
        if (tokenNode != null) {
            loginToken.setToken(tokenNode.getTextContent());
        }

        Node keyNode = getChildByName(loginTokenNode, "key");
        if (keyNode != null) {
            loginToken.setKey(keyNode.getTextContent());
        }

        Node hhidNode = getChildByName(loginTokenNode, "householdId");
        if (hhidNode != null) {
            loginToken.setHouseholdId(hhidNode.getTextContent());
        }

        return credentials;
    }

    /**
     * Convenience method for finding the first child node under a given parent, with the specified local name
     * @param parent the Node under which to search
     * @param localName the local name to look for
     * @return the first Node under parent mmatching localName, or null if none is found
     */
    private Node getChildByName(Node parent, String localName) {
        if (parent == null) {
            logger.warn("parent node is null?");
            return null;
        }

        NodeList list = parent.getChildNodes();
        if (list == null) {
            logger.warn("child node list is null?");
            return null;
        }

        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if (item != null && item.getLocalName() != null && item.getLocalName().equals(localName)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
    }
}
