# Copyright (c) 2015 Sonos, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

JENKINS_BUILD_NUMBER=$(shell echo $$BUILD_NUMBER)
APP_NAME=contentapi-sample-app
APP_VERSION=$(shell cat build.gradle | grep "def VERSION_MAJOR_MINOR" | cut -d '"' -f 2)
TARBALL=artifacts/$(APP_NAME)-src-$(APP_VERSION).$(JENKINS_BUILD_NUMBER).tar.gz

# uses gradle if on Jenkins, or gradlew locally
all: 
	if [ -z "${JENKINS_URL}" ] ; then \
		./gradlew clean build ; \
	else \
		export JAVA_HOME=/usr/lib/jvm/java-1.8.0-oracle ; \
		gradle clean build ; \
	fi

# packages artifacts after building into tarballs 
release: all $(TARBALL)

$(TARBALL):
	mkdir -p artifacts
	mkdir -p tmp/$(APP_NAME)
	tar --exclude="tmp" --exclude="build" --exclude="artifacts" --exclude=".idea" --exclude=".gradle" --exclude="loc.py" --exclude="loc-last-merged.xml" -cv * | tar -xv --directory tmp/$(APP_NAME)
	tar -zcvf $(TARBALL) --directory tmp $(APP_NAME)
	rm -rf tmp
#	tar -zcvf artifacts/lint-outputs.tar.gz --directory=app/build/ outputs
	tar -zcvf artifacts/junit-reports.tar.gz --directory=build/ reports
#	tar -zcvf artifacts/javadoc.tar.gz --directory=app/build/docs javadoc

# runs unit tests (uses gradle if on Jenkins, or gradlew locally)
test: clean
	if [ -z "${JENKINS_URL}" ] ; then \
		./gradlew cleanTest test ; \
	else \
		gradle cleanTest test ; \
	fi  

# removes existing artifacts from previous runs
clean:
	rm -rf tmp
	rm -rf artifacts
	rm -f src/resources/Sonos.wsdl

.PHONY: all release test clean
